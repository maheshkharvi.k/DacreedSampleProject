/**
 * Copyright CGI 2013
 * @author Prashant Grover
 */
package nz.co.telecom.facades.product;

import java.util.Collection;
import java.util.List;

import nz.co.telecom.core.integration.abs.services.data.ProductAvailabilityResponse;
import nz.co.telecom.facades.data.BundleTabData;
import nz.co.telecom.facades.data.LegacyAccess;
import nz.co.telecom.facades.data.SiteDetailsData;
import nz.co.telecom.facades.data.WirelessBroadband;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductResultData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;


public interface TNZProductFacade extends ProductFacade
{
	/**
	 * Populates the bundle and frequency tabs using either the default service plan or deep-linked selected plan for
	 * Device Details Page
	 * 
	 * @param productModel
	 * @param servicePlanId
	 * @return ProductData
	 */
	public ProductData getProductForOptions(final ProductModel productModel, final Collection<ProductOption> options,
			final String servicePlanId, final String bundleTemplateIds);
	
	public ProductData getProductForOptions(final ProductModel productModel, final Collection<ProductOption> options,
			final String servicePlanId, final String serviceAddOn, final String bundleTemplateIds);

}
