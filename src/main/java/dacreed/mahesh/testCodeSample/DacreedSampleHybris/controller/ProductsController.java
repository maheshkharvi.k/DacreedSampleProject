/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package nz.co.telecom.controller;

import nz.co.sparkdigital.facades.product.SparkDigitalProductExportFacade;
import nz.co.sparkdigital.facades.search.SparkDigitalProductSearchFacade;
import nz.co.sparkdigital.solr.search.data.SolrSearchData;
import nz.co.sparkdigital.solr.search.enums.BaseSiteEnum;
import nz.co.telecom.constants.YcommercewebservicesConstants;
import nz.co.telecom.core.model.DeviceModel;
import nz.co.telecom.core.model.ServiceAddOnModel;
import nz.co.telecom.core.model.ServicePlanModel;
import nz.co.telecom.core.util.CartUtils;
import nz.co.telecom.dto.ProductDataList;
import nz.co.telecom.dto.SuggestionDataList;
import nz.co.telecom.facade.search.ProductSearchFacade;
import nz.co.telecom.facades.order.impl.DefaultBundleCartFacade;
import nz.co.telecom.facades.product.TNZProductFacade;
import nz.co.telecom.formatters.WsDateFormatter;
import nz.co.telecom.util.ws.SearchQueryCodec;
import nz.co.telecom.validator.CustomValidationException;
import nz.co.telecom.validator.ReviewDataValidator;

import com.google.common.collect.Lists;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.httpclient.util.DateParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.ProductExportFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.ProductReferencesData;
import de.hybris.platform.commercefacades.product.data.ProductResultData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.data.SuggestionData;
import de.hybris.platform.commercefacades.search.data.AutocompleteSuggestionData;
import de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;


/**
 * Web Services Controller to expose the functionality of the {@link ProductFacade} and SearchFacade.
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/products")
public class ProductsController extends BaseController
{
   

    @Autowired
    private DefaultBundleCartFacade bundleCartFacade;
    
    @Resource(name = "cwsSparkDigitalProductExportFacade")
    private SparkDigitalProductExportFacade sparkDigitalProductExportFacade;
    
    @Autowired
    private SparkDigitalProductSearchFacade sparkDigitalProductSearchFacade;
    
    @Resource(name = "cwsSparkDigitalProductFacade")
    private ProductFacade sparkDigitalProductFacade;
    
    @Autowired
    private ConfigurationService configurationService;
      

    /**
     * Web service handler for the getProductByCode call. If no 'options' query parameter is defined, it will assume
     * BASIC. The options are turned into a Set<ProductOption> and passed on to the facade. Sample Call:
     * http://localhost:9001/rest/v1/{SITE}/products/{CODE}?options=BASIC%2CPROMOTIONS Keep in mind ',' needs to be
     * encoded as %2C
     * 
     * @param code
     *            - the unique code used to identify a product
     * @param options
     *            - a String enumerating the detail level, values are BASIC, PROMOTIONS, STOCK, REVIEW, CLASSIFICATION,
     *            REFERENCES. Combine by using a ',', which needs to be encoded as part of a URI using URLEncoding: %2C
     * @return the ProdcutData DTO which will be marshaled to JSON or XML based on Accept-Header
     * 
     * created for new changes to device detail page . Only for personal shop
     */
    
    @RequestMapping(value = "deviceDetail/{code}", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductData> getProductByCodeForDeviceDetail(@PathVariable
    final String code, @RequestParam(required = false, defaultValue = BASIC_OPTION)
    final String options, @RequestParam(required = false, defaultValue = "")
    final String servicePlanId, @RequestParam(value = "serviceAddOn", required = false)
    final String serviceAddOnId, @RequestParam(value = "bundleTemplateIds", required = false)
    final String bundleTemplateIds, @PathVariable("baseSiteId")
    final String baseSiteId) {

        final Set<ProductOption> opts = extractOptions(options);

        LOG.info("Product code : " + code + " Options : " + opts + " BaseSite Id :" + baseSiteId);
        final ProductModel productModel = productService.getProductForCode(code);

        if (opts.contains(ProductOption.BUNDLE_TABS)) {
            if (productModel instanceof DeviceModel) {
                opts.add(ProductOption.DEVICE_BUNDLE_TABS);
            } else if (productModel instanceof SubscriptionProductModel) {
                if (productModel instanceof ServicePlanModel) {
                    opts.add(ProductOption.SERVICE_PLAN_BUNDLE_TABS);
                } else if (productModel instanceof ServiceAddOnModel) {
                    opts.add(ProductOption.SERVICE_ADDON_BUNDLE_TABS);
                }
            }
        }
        LOG.info("getProductByCode :: Calling facade.getProductForOptions :: with Product : " + productModel.getCode()
                        + ", plan : " + servicePlanId + " and serviceAddOnId : " + serviceAddOnId);
        return productFacade.getProductForOptionsList(productModel, opts, servicePlanId, serviceAddOnId,
                        bundleTemplateIds);
    }
    
    

  
    /**
     * Setter for productFacade.
     * 
     * @param productFacade
     *            the value to set productFacade
     */
    public void setProductFacade(final TNZProductFacade productFacade)
    {
        this.productFacade = productFacade;
    }

    /**
     * Getter for productFacade.
     * 
     * @return the productFacade
     */
    public TNZProductFacade getProductFacade()
    {
        return productFacade;
    }
    
    public ConfigurationService getConfigurationService() {
		return configurationService;
	}
}
