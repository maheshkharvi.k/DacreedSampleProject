package nz.co.telecom.facades.product.impl;

import nz.co.telecom.core.enums.BundleType;
import nz.co.telecom.core.enums.PlanTypeEnum;
import nz.co.telecom.core.integration.abs.services.data.AddressRequestData;
import nz.co.telecom.core.integration.abs.services.data.CopperProduct;
import nz.co.telecom.core.integration.abs.services.data.Product;
import nz.co.telecom.core.integration.abs.services.data.ProductAvailabilityResponse;
import nz.co.telecom.core.interceptor.impl.DeviceAddOnCompatibilityValidator;
import nz.co.telecom.core.model.AccessoryModel;
import nz.co.telecom.core.model.DeviceModel;
import nz.co.telecom.core.model.InternetConnectionDetailsModel;
import nz.co.telecom.core.model.PhoneModel;
import nz.co.telecom.core.model.ServiceAddOnModel;
import nz.co.telecom.core.model.ServicePlanModel;
import nz.co.telecom.core.services.ClickToCartService;
import nz.co.telecom.core.services.impl.DefaultBroadbandAvailabilityService;
import nz.co.telecom.core.util.CartUtils;
import nz.co.telecom.core.util.ProductUtil;
import nz.co.telecom.facades.constants.TnzFacadesConstants;
import nz.co.telecom.facades.data.BundleTabData;
import nz.co.telecom.facades.data.FrequencyTabData;
import nz.co.telecom.facades.data.LegacyAccess;
import nz.co.telecom.facades.data.NetworkTechnologyType;
import nz.co.telecom.facades.data.SiteDetailsData;
import nz.co.telecom.facades.data.WirelessBroadband;
import nz.co.telecom.facades.mduservices.MDUInfoFacade;
import nz.co.telecom.facades.options.converters.populator.TNZDeliveryModePopulatorForProductDetail;
import nz.co.telecom.facades.order.converters.populator.ConnectionAddressPopulator;
import nz.co.telecom.facades.product.TNZProductFacade;
import nz.co.telecom.facades.product.TelcoProductFacade;
import nz.co.telecom.facades.product.converters.populator.TNZProductPopulator;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductResultData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;


public class TNZDefaultProductFacade<REF_TARGET> extends DefaultTelcoProductFacade implements TNZProductFacade
{
	
	private static final Logger LOG = Logger.getLogger(TNZDefaultProductFacade.class);
	@Autowired
	private TNZProductPopulator<ProductModel, ProductData, ProductOption> tnzProductPopulator;
	
	@Autowired
    private TNZDeliveryModePopulatorForProductDetail<ProductModel, de.hybris.platform.commercefacades.product.data.ProductData, BundleTemplateModel> deliveryModePopulatorForProductDetail;
	
    @Autowired
    private ProductService productService;
    
    @Autowired
    private DefaultBroadbandAvailabilityService defaultBroadbandAvailabilityService;
    
	@Autowired
    private ProductDao productDao;
    
    @Autowired
    private CatalogVersionService catalogVersionService;
    
    @Resource(name = "bundleTemplateService")
	private BundleTemplateService bundleTemplateService;
	
	@Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Autowired
    private MDUInfoFacade defaultMDUInfoFacade;

    @Autowired
    private BaseStoreService defaultBaseStoreService;

	@Override
	public ProductData getProductForOptions(final ProductModel productModel, final Collection<ProductOption> options,
			final String servicePlanId, final String bundleTemplateIds)
	{

		final ProductData productData = this.getProductForOptions(productModel, options, bundleTemplateIds);

		if ((productModel instanceof DeviceModel) && options.contains(ProductOption.DEVICE_BUNDLE_TABS))
		{
			getTnzProductPopulator().populate(productModel, productData, servicePlanId);
		}
		else if(productModel instanceof AccessoryModel)
		{
            final BundleTemplateModel accessoryBundleTemplate =
                            clickToCartService.getBundleTemplateForProductAndBundleType(productModel,
                                            BundleType.ACCESSORY_ONLY);
		    if(accessoryBundleTemplate != null)
		    {
	            productData.setDeviceOnlyComponentId(accessoryBundleTemplate.getId());
		    }
		}

		return productData;
	}
	

    @Override
    public List<ProductData> getProductForOptionsList(final ProductModel productModel,
            final Collection<ProductOption> options, final String servicePlanId, final String serviceAddOn,
            final String bundleTemplateIds) {

        final List<ProductData> productDataList = new ArrayList<>();
        List<ProductModel> productModels = ProductUtil.getReferenceListProductByType(productModel);
        for (ProductModel prodReference : productModels) {
            productDataList.add(getProductForOptions(prodReference, options, servicePlanId, serviceAddOn,
                    bundleTemplateIds));
        }
        return productDataList;
    }


	@Override
  public ProductData getProductForOptions(final ProductModel productModel, final Collection<ProductOption> options,
			String servicePlanId, String serviceAddOn, final String bundleTemplateIds) {
		
		final ProductData productData = getProductForOptions(productModel, options, servicePlanId, bundleTemplateIds);
		if(StringUtils.isEmpty(serviceAddOn)) {
			if(!StringUtils.isEmpty(servicePlanId)) {
				final ServicePlanModel servicePlanModel = (ServicePlanModel) productService.getProductForCode(servicePlanId);
				ServiceAddOnModel serviceAddOnModel= null;
				if(servicePlanModel != null) {
					if(productModel instanceof DeviceModel){
						serviceAddOnModel = ((DeviceModel) productModel).getDefaultServiceAddOn();
						final boolean isCompatible = deivceAddonCompatibilitValidator.isCompatibleWithDevice((DeviceModel)productModel, serviceAddOnModel);
						if(!isCompatible ||serviceAddOnModel==null){
							LOG.info("Service Addon is not compatible with Device hence selecting default service add-on from plan: deviceModel"+productModel.getCode());
							serviceAddOnModel=servicePlanModel.getDefaultServiceAddOn();
						}
					}
					else{
						serviceAddOnModel = servicePlanModel.getDefaultServiceAddOn();
					}
					
					if(serviceAddOnModel == null) {
						LOG.warn("THERE IS NO DEFAULT SERVICE ADDON CONFIGURED FOR PLAN : "+servicePlanModel.getCode()+". HENCE NO SERVICE ADDON WILL BE PRESELECTED AS DEFAULT");
					} else {												
						serviceAddOn = serviceAddOnModel.getCode();
						LOG.info("Setting serviceAddOn :"+serviceAddOn+" as preselected, since serviceAddOn is not supplied");
					}
				} else {
					//LOG.warn("THERE IS NO SERVICE PLAN AVAILABLE HERE. DONT KNOW WHAT TO DO ???");
				}
			} else {
			      if(CollectionUtils.isNotEmpty(productData.getBundleTabs())){
	                //1. get the 1st prepay plan from ProductData
	                //2. Get the Plan Model for the code
	                //3
	                final ProductData planData = getFirstPrepayPlanForProductData(productData.getBundleTabs());
	                if(planData != null) {
	                    final ServicePlanModel servicePlanModel = (ServicePlanModel) productService.getProductForCode(planData.getCode());
	                    if(servicePlanModel != null) {
	                        ServiceAddOnModel serviceAddOnModel= null;
	                        if(productModel instanceof DeviceModel){
	                            serviceAddOnModel = ((DeviceModel) productModel).getDefaultServiceAddOn();
	                            final boolean isCompatible = deivceAddonCompatibilitValidator.isCompatibleWithDevice((DeviceModel)productModel, serviceAddOnModel);
	                            if(!isCompatible ||serviceAddOnModel==null){
	                                LOG.info("Service Addon is not compatible with Device hence selecting default service add-on from plan: deviceModel"+productModel.getCode());
	                                serviceAddOnModel=servicePlanModel.getDefaultServiceAddOn();
	                            }
	                        }
	                        else{
	                            serviceAddOnModel = servicePlanModel.getDefaultServiceAddOn();
	                        }
	                        if(serviceAddOnModel == null) {
	                            LOG.warn("THERE IS NO DEFAULT SERVICE ADDON CONFIGURED FOR PLAN : "+servicePlanModel.getCode()+". HENCE NO SERVICE ADDON WILL BE PRESELECTED AS DEFAULT");
	                        } else {                                                
	                            serviceAddOn = serviceAddOnModel.getCode();
	                            servicePlanId = servicePlanModel.getCode();
	                            LOG.info("Setting serviceAddOn :"+serviceAddOn+" as preselected, since serviceAddOn is not supplied");
	                        }
	                    }
	                }
	             }
			}
		}
		getTnzProductPopulator().populate(productModel, productData, servicePlanId, serviceAddOn);
		BundleTemplateModel bundleTemplate = null;
		if (StringUtils.isNotEmpty(bundleTemplateIds)) {
		    bundleTemplate = bundleTemplateService.getBundleTemplateForCode(bundleTemplateIds);
		}
		getDeliveryModePopulatorForProductDetail().populate(productModel, productData, bundleTemplate);
		return productData;		
	}

	
	private ProductData getFirstPrepayPlanForProductData(final List<BundleTabData> bundleTabs) {
		for (final BundleTabData bundleTab : bundleTabs) {
			if(bundleTab.getBundleType().getCode().equalsIgnoreCase("prepaid")) {
				for (final FrequencyTabData frequencyTab : bundleTab.getFrequencyTabs()) {
					final ProductData planData = frequencyTab.getProducts().get(0);
					return planData;
				}
			}
		}
		return null;
	}

	