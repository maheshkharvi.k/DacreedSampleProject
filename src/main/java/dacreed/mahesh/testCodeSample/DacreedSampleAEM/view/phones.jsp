
<tnz:useBean id="deviceDetail" scope="request"
	beanClass="nz.co.tnz.components.pages.DeviceDetailPage" />
<tnz:useBean id="cart" scope="request"
	beanClass="nz.co.tnz.components.shop.checkout.CartComponent" />
<tnz:useBean id="pageproperties" scope="request"
	beanClass="nz.co.tnz.base.core.components.PagePropertiesComponent" />
<tnz:useBean id="filterComponent" scope="request"
	beanClass="nz.co.tnz.components.shop.DeviceFilterComponent" />
<input type="hidden" id="voucherApplied"
	value=${cart.isVoucherAlreadyApplied}>
<c:set var="isAddOnAvailable" value="no" />
<c:set var="productsList" value="${deviceDetail.productsList}" />
<c:set var="isOutOfStock" value="false" />
<input type="hidden" id="analyticsData"
	value='${deviceDetail.analyticsData}' />

<c:if test="${deviceDetail.tradeUpJourney}">
	<input type="hidden" id="isTradeUpJourney" value="true" />
</c:if>

<form action="#">
	<c:forEach items="${param}" var="par">
		<c:set var="parKey">${par.key}</c:set>
		<c:set var="parValue">${par.value}</c:set>
		<input type="hidden"
			name="<%=xssAPI.encodeForHTML((String) pageContext.getAttribute("parKey"))%>"
			value="<%=xssAPI.encodeForHTML((String) pageContext.getAttribute("parValue"))%>">
	</c:forEach>
</form>



<script>
	//Device details page - Direct call rules
	var allowedFlag = false;

	function populateAddEntryData(activeSubmit, event) {
		if (allowedFlag === false) {
			if (activeSubmit === 'prepaid') {
				digitalData.shop.mobile.planID = $(
						"[data-dtm='prepaidSubmit'] :input[name='productCode2']")
						.val();
				digitalData.shop.mobile.planName = $(
						"[data-dtm='prepaidSubmit'] :input[name='planName']")
						.val();
				digitalData.shop.mobile.planType = $(
						"[data-dtm='prepaidSubmit'] :input[name='planType']")
						.val();
				delete digitalData.shop.mobile.planPricePerMonth;
				delete digitalData.shop.mobile.interestFreePayments;
			} else if (activeSubmit === 'postpaid') {
				digitalData.shop.mobile.planID = $(
						"[data-dtm='postpaidSubmit'] :input[name='productCode2']")
						.val();
				digitalData.shop.mobile.planName = $(
						"[data-dtm='postpaidSubmit'] :input[name='planName']")
						.val();
				digitalData.shop.mobile.planType = $(
						"[data-dtm='postpaidSubmit'] :input[name='planType']")
						.val();
				digitalData.shop.mobile.planPricePerMonth = $(
						"[data-dtm='postpaidSubmit'] :input[name='planPricePerMonth']")
						.val();
				var selectedDeferLength = $(
						"input[name='additionalcost']:checked").attr(
						'data-defer-length');
				var selectedDeferFrequency = $(
						"input[name='additionalcost']:checked").attr(
						'data-defer-frequency');
				if (selectedDeferLength !== undefined
						&& selectedDeferFrequency !== undefined) {
					digitalData.shop.mobile.interestFreePayments = selectedDeferLength
							+ selectedDeferFrequency;
				}
			}

			if (typeof _satellite !== 'undefined' && _satellite != null) {
				_satellite.track("shop.pageComplete");
				_satellite.track("scCheckout");
			}
			setTimeout(function() {
				if (activeSubmit === 'prepaid') {
					$("[data-dtm='prepaidSubmit']").submit();
				} else if (activeSubmit === 'postpaid') {
					$("[data-dtm='postpaidSubmit']").submit();
				}

			}, 100);
		} else {
			return true;
		}

		allowedFlag = true;
		event.stopPropagation();
		return false;
	}

	function populateDeviceOnlyData() {
		if (allowedFlag === false) {
			if (typeof _satellite !== 'undefined' && _satellite != null) {
				_satellite.track("shop.pageComplete");
				_satellite.track("scCheckout");
			}
			setTimeout(function() {
				$("[data-dtm='buy-device-only']").submit();
			}, 100);
		} else {
			return true;
		}

		allowedFlag = true;
		event.stopPropagation();
		return false;
	}
</script>


<div class="details-page responsive-gallery-product-detail-wrapper">
	<div class="container-fluid">
		<div class="row visible-md visible-lg" />
	</div>
	<div class="row">
		<tnz:include path="why-buy-spark"
			resourceType="spark-responsive/components/why-spark" />
		<tnz:include path="mainbanners" resourceType="tnz/components/parsys" />
		<c:forEach var="devicelist" items="${deviceDetail.deviceDetailList}"
			varStatus="statusCartEntry">
			<c:if test="${deviceDetail.baseProductSKU eq devicelist.deviceId}">
				<form data-dtm="postpaidSubmit" action="${currentPage.path}.commerce.addcartentrytnz.html" method="post" onSubmit="populateAddEntryData('postpaid',event)">
					<input type="hidden" id="" value="" class="deferSelected">
					<input type="hidden" value="" class="deferSelectedvalue" name="deferPaymentCode"> 
				    <input type="hidden" value="" class="deferSelectedId" name="deferPaymentBundleTemplateId">
					<input type="hidden" name="manufacturer" value="${devicelist.manufacturer}"> 
					<input type="hidden" name="productCode1" value="${deviceDetail.baseProductSKU}" class="selectedProducts">
                     <input type="hidden" name="productCode2" value="${devicelist.currentPostpaidPlan}" class="selectedProducts">
                     <input type="hidden" name="planName" value="${devicelist.planName}"> 
                     <input type="hidden" name="planType" value="${devicelist.planType}">
					 <input type="hidden" name="deviceName" value="${devicelist.deviceName}"> 
					 <input type="hidden" name="planPricePerMonth" value="${devicelist.postpaidPlanPrice}">
					 <input type="hidden" name="bundleTemplate1" value="${devicelist.bundleTemplate1}">
					 <input type="hidden" name="bundleTemplate2" value="${devicelist.bundleTemplate2}">
					 <input type="hidden" name="loginPreSelection" value="">
					 <input type="hidden" name="redirect" value="${deviceDetail.addToCartRedirect}.html" />
					 <input type="hidden" name="baseStore" value="${deviceDetail.baseStore}">
					 <input type="hidden" name="redirect-product-not-found" value="${deviceDetail.cartErrorRedirect}.html" /> 
					 <input type="hidden" name="journey" value="mobileJourney" /> 
					 <input type="hidden" name="tradeUpBundleId" value="${deviceDetail.tradeUpBundleId}" /> 
					  <input type="hidden" name="journeyType" value="${param.journeyType}" /> 
					  <input type="hidden" name="subJourneyType" value="${param.subJourneyType}" />
				</form>

				<form data-dtm="prepaidSubmit"
					action="${currentPage.path}.commerce.addcartentrytnz.html"
					method="post" onSubmit=" populateAddEntryData('prepaid',event);">
					 <input
						type="hidden" name="productCode1"
						value="${deviceDetail.baseProductSKU}"> <input
						type="hidden" name="productCode2"
						value="${devicelist.currentPrepaidPlan}"> <input
						type="hidden" name="planName"
						value="${devicelist.prepaidServicePlanName}"> <input
						type="hidden" name="planType"
						value="${devicelist.prepaidPlanType}"> <input
						type="hidden" name="bundleTemplate1"
						value="${devicelist.prepaidBundleTemplate1}"> <input
						type="hidden" name="bundleTemplate2"
						value="${devicelist.prepaidBundleTemplate2}"> <input
						type="hidden" name="baseStore" value="${deviceDetail.baseStore}">
					<input type="hidden" name="loginPreSelection" value=""> <input
						type="hidden" name="redirect"
						value="${deviceDetail.addToCartRedirect}.html" /> <input
						type="hidden" name="redirect-product-not-found"
						value="${deviceDetail.cartErrorRedirect}.html" /> <input
						type="hidden" name="journey" value="mobileJourney" /> <input
						type="hidden" name="journeyType" value="${param.journeyType}" />
					<input type="hidden" name="subJourneyType"
						value="${param.subJourneyType}" /> <input type="hidden"
						name="compatibleAddOn" value="${devicelist.compatibleAddOn}" /> <input
						type="hidden" name="compatibleAddOnTemplateId"
						value="${devicelist.compatibleAddOnTemplateId}" />
				</form>

				<c:if
					test="${devicelist.deviceSoldIndividually && !deviceDetail.tradeUpJourney}">
					<form data-dtm="buy-device-only"
						action="${currentPage.path}.commerce.addcartentrytnz.html"
						method="post">
						 <input type="hidden"
							name="productCode" value="${deviceDetail.baseProductSKU}" /> <input
							type="hidden" name="bundleTemplate"
							value="${devicelist.deviceOnlyComponentId}" /> <input
							type="hidden" name="baseStore" value="${deviceDetail.baseStore}" />
						<input type="hidden" name="redirect"
							value="${deviceDetail.addToCartRedirect}.html" /> <input
							type="hidden" name="loginPreSelection" value=""> <input
							type="hidden" name="redirect-product-not-found"
							value="${deviceDetail.cartErrorRedirect}.html" /> <input
							type="hidden" name="journey" value="deviceOnlyJourney" /> <input
							type="hidden" name="journeyType" value="${param.journeyType}" />
						<input type="hidden" name="subJourneyType"
							value="${param.subJourneyType}" />
					</form>
				</c:if>
			</c:if>

		</c:forEach>


		<div class="col-xs-12 col-sm-12 col-md-4">
			<c:forEach var="deviceId" items="${productsList}"
				varStatus="deviceCount">
				<c:set var="deviceIdentifier" value="${deviceId}" scope="request" />
				<c:choose>
					<c:when test="${deviceCount.index eq '0'}">
						<cq:include path="imagegallery"
							resourceType="tnz/components/shop/imagegallery" />
						<input type="hidden" name="deviceImgVariants"
							value="${deviceImgVariants}">
						<spark-product-detail-device-images
							prop-device-img-variants='{"${deviceId}":${deviceImgVariants}}'>
						</spark-product-detail-device-images>
					</c:when>
					<c:otherwise>
						<cq:include path="imagegallery-${deviceId}"
							resourceType="tnz/components/shop/imagegallery" />
						<spark-product-detail-device-images
							prop-device-img-variants='{"${deviceId}": ${deviceImgVariants}}'>
						</spark-product-detail-device-images>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<spark-product-detail-one-detail
				prop-detail='${deviceDetail.deviceDetailListJson}'
				prop-do-we-know-eachother-content="${doWeKnowEachOther}"
				prop-journey-type="${param.journeyType}"
				prop-istradeup="${deviceDetail.tradeUpJourney}"
				prop-12MonthPromoBanner="${deviceDetail.promo12Month}"
				prop-12MonthPromoMessage="${deviceDetail.promoMessage12Month}"
				prop-24MonthPromoBanner="${deviceDetail.promo24Month}"
				prop-24MonthPromoMessage="${deviceDetail.promoMessage24Month}">
			</spark-product-detail-one-detail>
		</div>

		<script type="application/Id+json">
        ${deviceDetail.seoDataJson}
        </script>

		<div class="col-xs-12 col-sm-12 col-md-8 product-detail-rhs">
			<spark-product-detail-header></spark-product-detail-header>
			<tnz:include path="overview-tech"
				resourceType="spark-responsive/components/overview-tech-details" />
			<spark-product-detail-configurator>
			</spark-product-detail-configurator>
			<spark-product-detail-submit>
			<div class="cq-out-of-stock-message" id="cq-out-of-stock-message">
				<c:if test="${pageproperties.editMode eq true}">
					<label>This is custom HTML block for Out of Stock - Use for
						authors Only </label>
				</c:if>
				<tnz:include path="outOfStock_managed_area"
					resourceType="/apps/telecomcms/components/customhtml" />
			</div>

			<tnz:include path="beforeSubmit" resourceType="tnz/components/parsys" />
			</spark-product-detail-submit>
			<div class="clearfix padding-bottom-15"></div>
			<tnz:include path="afterSubmit" resourceType="tnz/components/parsys" />
			<tnz:include path="responsive-accordion"
				resourceType="/apps/spark-responsive/components/responsive-accordion" />
		</div>
	</div>
</div>