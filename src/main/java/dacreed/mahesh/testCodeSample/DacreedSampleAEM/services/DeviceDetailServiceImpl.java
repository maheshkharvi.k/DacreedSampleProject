package nz.co.spark.components.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.jcr.vault.util.JcrConstants;
import com.google.gson.Gson;

import nz.co.spark.base.core.utils.URLParamConstants;
import nz.co.spark.base.services.EntitlementService;
import nz.co.spark.commerce.hybris.HybrisConstants;
import nz.co.spark.commerce.hybris.HybrisSession;
import nz.co.spark.commerce.hybris.to.BundleTabTO;
import nz.co.spark.commerce.hybris.to.ChoosePlanEntry;
import nz.co.spark.commerce.hybris.to.ClassificationTO;
import nz.co.spark.commerce.hybris.to.CreditsTO;
import nz.co.spark.commerce.hybris.to.DeferredPaymentTO;
import nz.co.spark.commerce.hybris.to.DeviceDetailListTO;
import nz.co.spark.commerce.hybris.to.DeviceDetailTO;
import nz.co.spark.commerce.hybris.to.EntitlementsTO;
import nz.co.spark.commerce.hybris.to.FrequencyTabProductTO;
import nz.co.spark.commerce.hybris.to.FrequencyTabTO;
import nz.co.spark.commerce.hybris.to.PriceInfoTNZ;
import nz.co.spark.commerce.hybris.to.ProductRefGroupTO;
import nz.co.spark.commerce.hybris.to.ProductReferenceGroup;
import nz.co.spark.commerce.hybris.to.ProductTO;
import nz.co.spark.commerce.hybris.to.ServiceAddOnProductTO;
import nz.co.spark.commerce.hybris.to.StockInfo;
import nz.co.spark.commerce.hybris.to.Target;
import nz.co.spark.commerce.hybris.to.UsageChargeTO;
import nz.co.spark.commerce.hybris.to.checkout.Rule;
import nz.co.spark.components.services.AddToCartService;
import nz.co.spark.components.services.ChangePlanWithDeviceService;
import nz.co.spark.components.services.DeviceDetailService;
import nz.co.spark.components.services.ShopLoginService;
import nz.co.spark.components.transferobjects.CPWithDeviceData;
import nz.co.telecom.pasl.iam.model.Identity;
import nz.co.tnz.base.core.utils.Constants;
import nz.co.tnz.base.services.ContentAdminSessionFactory;
import nz.co.tnz.base.services.HybrisRequestHeader;
import nz.co.tnz.components.pages.DeviceDetailPage;
import nz.co.tnz.components.pages.ShopConfigPage;

/**
 * This class implements the Device Detail Page, which represents the Products.
 * 
 * @author Mahesh Kharvi
 * 
 */
@Service
@Component(label = "Device Detail Service", metatype = false, immediate = true)

public class DeviceDetailServiceImpl implements DeviceDetailService {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceDetailPage.class);

    /** services to make a call to Hybris **/


    /** constants **/
    private final static String PREPAID_FREQUENCY = "prepaid";
    public static final String ENTITLEMENT_DAM_PATH = "/content/dam/tnz/serviceplan/devicedetail/";
    public static final String PREPAID_ENTITLEMENT_DAM_PATH = "/content/dam/tnz/valuepacks/devicedetail/";
    private String POSTPAID_BUNDLE = "Pay Monthly";
    private String OUT_OF_STOCK = "outOfStock";
    private String OUT_OF_STOCK_MESSAGE = "Out of Stock.Please try again after";
    private String UNAVAILABLE_MESSAGE = "Sorry, not currently available for postpaid";
    private String PREPAID_UNAVAILABLE_MESSAGE = "Sorry, not currently available for prepaid";
    private String TRADEUP_BUNDLE_ID = "tradeUpBundleId";
    private int[] otherTermplans = {12, 24};
    private int otherTermName = 1224;
    public final String pagePath = "/content/cms/sample/en/store/tnzProducts/mobile/phones";


    /** Additional properties */
    private ShopConfigPage config = null;



    @Reference
    private ContentAdminSessionFactory adminSessionFactory;

    @Reference
    private EntitlementService entitlementService;

    @Reference
    private ChangePlanWithDeviceService changePlanWithDeviceService;

    @Reference
    private ShopLoginService shopLoginService;

    @Reference
    private AddToCartService addToCartService;

    @Reference
    private QueryBuilder builder;

    @Override
    public List<DeviceDetailListTO> getDeviceDetailData(SlingHttpServletRequest request,
            HybrisRequestHeader hybrisRequestHeader, SlingHttpServletResponse response) throws Exception
    {
        String baseStore = null;

        final List<String> deviceList = new ArrayList<>();
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = null;
        CommerceService commerceService = setCommerceService(request);
        HybrisSession hybrisSession = setCommerceSession(request, response, commerceService);
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        com.day.cq.wcm.api.Page currentPage = setCurrentPage(request, pageManager);
        Product baseProduct = CommerceHelper.findCurrentProduct(currentPage).adaptTo(Product.class);
        baseStore = baseProduct.getProperty(HybrisConstants.PROPERTY_HYBRIS_BASE_STORE, String.class);

        String productTrackingPath = baseProduct.getProperty("productData", String.class);
        if (productTrackingPath == null) {
            productTrackingPath = baseProduct.getPagePath();
        }
        request.setAttribute("cq.commerce.productTrackingPath", productTrackingPath);
        request.setAttribute("cq.commerce.productPagePath", baseProduct.getPagePath());

        List<DeviceDetailListTO> deviceDetailTOList = new ArrayList<>();

        try {
            session = this.adminSessionFactory.createSession();
            List<JSONObject> deviceDetailTOJsonParser = hybrisSession.retrieveDeviceDetailTO(baseProduct,
                    session, baseStore, hybrisRequestHeader);
            for (JSONObject jsonParseProduct : deviceDetailTOJsonParser) {

                String deviceDetailsJsonString = jsonParseProduct.toString();
                LOG.info("deviceDetailsJsonString = " + deviceDetailsJsonString);
                Gson gson = new Gson();

                DeviceDetailTO deviceDetailTO = new DeviceDetailTO();
                DeviceDetailListTO deviceDetailListTO = new DeviceDetailListTO();

                deviceDetailTO = gson.fromJson(deviceDetailsJsonString, DeviceDetailTO.class);
                FrequencyTabProductTO frequencyTabPostpaidProductTO = findPostPaidProduct(deviceDetailTO);
                /*
                 * this.dataLayerObject =
                 * sparkDataLayerService.createDLDeviceDetails(deviceDetailTO, this.baseProduct,
                 * getDeviceRRP(deviceDetailTO), frequencyTabPostpaidProductTO);
                 * getRequest().getSession().setAttribute(Constants.DL_DATALAYEROBJECT,
                 * this.dataLayerObject);
                 */
                if (deviceDetailTO.getCode().equals(baseProduct.getSKU())) {
                    deviceDetailListTO.setBaseProductSKU(baseProduct.getSKU());
                    deviceDetailListTO.setBaseStore(baseStore);
                }
                deviceDetailListTO.setCustomerIdentified(
                        getIsCustomerIdentified(request, hybrisRequestHeader, response));
                deviceDetailListTO.setCategory(StringUtils.isNotEmpty(deviceDetailTO.getItemType()) ? deviceDetailTO.getItemType().toLowerCase() : "");
                if (null != frequencyTabPostpaidProductTO) {

                    config = new ShopConfigPage(baseStore, request.getResource());
                    deviceDetailTO.setSummary(
                            baseProduct.getProperty(JcrConstants.JCR_DESCRIPTION, String.class));

                deviceDetailListTO.setDeviceTitle(deviceDetailTO.getManufacturer() + " " + deviceDetailTO.getName());
                deviceDetailListTO.setDeviceManufacturer(deviceDetailTO.getManufacturer());
                setShortDescription(deviceDetailTO, deviceDetailListTO, baseProduct);
                generateChoosePlanData(deviceDetailTO, deviceDetailListTO, isMbb(request));
                generatePrepaidChoosePlanData(deviceDetailTO, deviceDetailListTO);
                getPostpaidEntitlementsJson(deviceDetailTO, deviceDetailListTO);
                getPrepaidProduct(deviceDetailTO, deviceDetailListTO);
                getOriginalColourJson(deviceDetailTO, deviceDetailListTO);
                getOriginalDeviceSize(deviceDetailTO, deviceDetailListTO);
                getOtherSizeJson(deviceDetailTO, deviceDetailListTO, request);
                getOtherColourJson(deviceDetailTO, deviceDetailListTO, request);
                getPrepaidChooseServiceAddOnJson(deviceDetailListTO);
                getDefaultPrepaidPlanAddOnAvailable(deviceDetailListTO);
                getCreditRuleJson(deviceDetailTO, deviceDetailListTO);
                getGiftRuleJson(deviceDetailTO, deviceDetailListTO);
                getCreditProductsAsJson(deviceDetailTO, deviceDetailListTO);
                getGiftProductsAsJson(deviceDetailTO, deviceDetailListTO);
                setManufacturer(deviceDetailTO, deviceDetailListTO);
                setBundleTemplates(deviceDetailTO, deviceDetailListTO);
                filterPlansForChangePlanJourney(deviceDetailListTO, request);
                deviceDetailListTO.setImagesList(getReferencePath(pagePath, deviceDetailListTO.getDeviceId(), session));
                deviceDetailTOList.add(deviceDetailListTO);

                LOG.info("Start : Parsing the data through JSON Parser" + System.currentTimeMillis());
            }
            // TODO Auto-generated method stub
            return deviceDetailTOList;
        } catch (RepositoryException e) {
            LOG.error("when execute retrieveDeviceDetailTO: " + e);
        } catch (Exception e) {
            LOG.error("Exception", e);
        } finally {
            if (session != null) {
                session.logout();
            }
        }
        return null;
    }

    @Override
    public String getTradeUpBundleID(SlingHttpServletRequest request) throws Exception
    {
        String tradeUpBundleId = null;
        if (request.getParameter(TRADEUP_BUNDLE_ID) != null) {
            tradeUpBundleId = (request.getParameter(TRADEUP_BUNDLE_ID));
        }
        return tradeUpBundleId;
    }


    /** @return the Page object of the current page */
    private com.day.cq.wcm.api.Page setCurrentPage(SlingHttpServletRequest request, PageManager pageManager)
    {
        com.day.cq.wcm.api.Page currentPage = null;
        if (request != null) {
            if (getComponentContext(request) != null) {
                currentPage = getComponentContext(request).getPage();
            } else {
                currentPage = pageManager.getContainingPage(request.getResource());
            }
        }
        return currentPage;
    }

    /** @return The componentContext */
    private final ComponentContext getComponentContext(SlingHttpServletRequest request)
    {
        return WCMUtils.getComponentContext(request);
    }

    private void setManufacturer(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        if (StringUtils.isNotEmpty(deviceDetailTO.getManufacturer())) {
            deviceDetailListTO.setManufacturer(deviceDetailTO.getManufacturer());
        }
    }

    private FrequencyTabProductTO findPostPaidProduct(final DeviceDetailTO deviceDetailTO)
    {
        // find the current postpaid product.
        FrequencyTabProductTO product = null;
        if (!getBundleTabsMap(deviceDetailTO).isEmpty()
                && getBundleTabsMap(deviceDetailTO).get("Pay Monthly").isPreselected()) {
            product = getPreselectedProduct(deviceDetailTO);
        } else {
            if (getDefaultServicePlanAvailable(deviceDetailTO)) {
                product = getDefaultPostPaidProduct(deviceDetailTO);
            } else {
                product = getFirstPostpaidProduct(deviceDetailTO);
            }

        }
        return product;
    }

    private Map<String, BundleTabTO> getBundleTabsMap(final DeviceDetailTO deviceDetailTO)
    {
        Map<String, BundleTabTO> bundleTabs = deviceDetailTO.getBundleTabs();
        if (deviceDetailTO != null && bundleTabs != null && !bundleTabs.isEmpty()) {
            return bundleTabs;
        } else {
            return Collections.emptyMap();
        }
    }

    private FrequencyTabProductTO getPreselectedProduct(final DeviceDetailTO deviceDetailTO)
    {
        FrequencyTabTO ftab = getSelectedFrequency(deviceDetailTO);
        if (ftab != null) {
            for (FrequencyTabProductTO product : ftab.getFrequencyTabProductList()) {
                if (product.isPreselected()) {
                    return product;
                }
            }
        } else {
            LOG.error("default values for product details");
            FrequencyTabProductTO defaultValue = new FrequencyTabProductTO();
            defaultValue.setPreselected(true);
            defaultValue.description = "default description";
            Map<String, EntitlementsTO> entMap = new HashMap<String, EntitlementsTO>();
            entMap.put("Talk", new EntitlementsTO("min", "Anytime mins", "voice", 150, ""));
            entMap.put("Text", new EntitlementsTO("sms", "TXTs", "text", 600, ""));
            entMap.put("Data", new EntitlementsTO("data", "data", "data", -1, ""));
            defaultValue.setEntitlementMap(entMap);
            defaultValue.name = "default term";
            defaultValue.otherBundleProductPrice = new PriceInfoTNZ(new BigDecimal("249.00"),
                    Currency.getInstance(Locale.US), "249.00 $", "$");
            defaultValue.thisBundleProductPrice =
                    new PriceInfoTNZ(new BigDecimal("2.50"), Currency.getInstance(Locale.US), "2.50 $", "$");
            return defaultValue;
        }
        // TODO: DEFAULT VERHALTEN ANPASSEN
        return null;
    }

    private void setBundleTemplates(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        if (null != getBundleTabsMap(deviceDetailTO).get("Pay Monthly")) {
            deviceDetailListTO.setBundleTemplate1(
                    getBundleTabsMap(deviceDetailTO).get("Pay Monthly").getSourceComponentID());
            deviceDetailListTO.setBundleTemplate2(
                    getBundleTabsMap(deviceDetailTO).get("Pay Monthly").getTargetComponentID());
        }

        if (null != getBundleTabsMap(deviceDetailTO).get("Prepaid")) {
            deviceDetailListTO.setPrepaidBundleTemplate1(
                    getBundleTabsMap(deviceDetailTO).get("Prepaid").getSourceComponentID());
            deviceDetailListTO.setPrepaidBundleTemplate2(
                    getBundleTabsMap(deviceDetailTO).get("Prepaid").getTargetComponentID());
        }
    }

    private boolean getDefaultServicePlanAvailable(final DeviceDetailTO deviceDetailTO)
    {

        boolean isDefaultServicePlanAvailable = false;
        Map<String, BundleTabTO> bundleTabs;
        if (StringUtils.isNotEmpty(deviceDetailTO.getDefaultServicePlan())) {

            bundleTabs = deviceDetailTO.getBundleTabs();
            if (!bundleTabs.isEmpty()) {
                for (BundleTabTO bundleTabTO : bundleTabs.values()) {
                    // only extract the prepaid plan data.
                    if (!StringUtils.equalsIgnoreCase(bundleTabTO.getType(), PREPAID_FREQUENCY)) {
                        List<FrequencyTabTO> frequencyTabs = bundleTabTO.getFrequencyTabs();
                        if (frequencyTabs != null) {
                            for (int j = 0; j < frequencyTabs.size(); j++) {
                                FrequencyTabTO frequencyTabTO = frequencyTabs.get(j);
                                List<FrequencyTabProductTO> frequencyTabProductList =
                                        frequencyTabTO.getFrequencyTabProductList();
                                if (frequencyTabProductList != null) {
                                    for (int k = 0; k < frequencyTabProductList.size(); k++) {
                                        FrequencyTabProductTO frequencyTabProductTO = frequencyTabProductList.get(k);

                                        // This is also hardcoded to "No Contract".
                                        if (frequencyTabProductTO.getFrequencyCode()
                                                .equalsIgnoreCase(deviceDetailTO.getDefaultServicePlan())) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return isDefaultServicePlanAvailable;
    }

    /**
     * Returns the default service plan sent by hybris
     * 
     * @return
     */
    private FrequencyTabProductTO getDefaultPostPaidProduct(final DeviceDetailTO deviceDetailTO)
    {
        Map<String, BundleTabTO> bundleTabs;
        if (StringUtils.isNotEmpty(deviceDetailTO.getDefaultServicePlan())) {

            bundleTabs = deviceDetailTO.getBundleTabs();
            if (!bundleTabs.isEmpty()) {
                for (BundleTabTO bundleTabTO : bundleTabs.values()) {
                    // only extract the prepaid plan data.
                    if (!StringUtils.equalsIgnoreCase(bundleTabTO.getType(), PREPAID_FREQUENCY)) {
                        List<FrequencyTabTO> frequencyTabs = bundleTabTO.getFrequencyTabs();
                        if (frequencyTabs != null) {
                            for (int j = 0; j < frequencyTabs.size(); j++) {
                                FrequencyTabTO frequencyTabTO = frequencyTabs.get(j);
                                List<FrequencyTabProductTO> frequencyTabProductList =
                                        frequencyTabTO.getFrequencyTabProductList();
                                if (frequencyTabProductList != null) {
                                    for (int k = 0; k < frequencyTabProductList.size(); k++) {
                                        FrequencyTabProductTO frequencyTabProductTO = frequencyTabProductList.get(k);

                                        // This is also hardcoded to "No Contract".
                                        if (frequencyTabProductTO.getFrequencyCode()
                                                .equalsIgnoreCase(deviceDetailTO.getDefaultServicePlan())) {
                                            return frequencyTabProductTO;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return null;
    }

    private FrequencyTabProductTO getFirstPostpaidProduct(final DeviceDetailTO deviceDetailTO)
    {
        FrequencyTabProductTO productTO = null;
        if (null != getBundleTabsMap(deviceDetailTO).get("Pay Monthly") && CollectionUtils
                .isNotEmpty(getBundleTabsMap(deviceDetailTO).get("Pay Monthly").getFrequencyTabs())) {
            productTO = getBundleTabsMap(deviceDetailTO).get("Pay Monthly").getFrequencyTabs().get(0)
                    .getFrequencyTabProductList().get(0);
        }
        return productTO;
    }

    private FrequencyTabTO getSelectedFrequency(final DeviceDetailTO deviceDetailTO)
    {
        BundleTabTO bTab = getSelectedBundleTab(deviceDetailTO);
        if (bTab != null && bTab.isPreselected()) {
            List<FrequencyTabTO> frequencyTabs = bTab.getFrequencyTabs();
            if (frequencyTabs != null && !frequencyTabs.isEmpty()) {
                for (FrequencyTabTO fTab : frequencyTabs) {
                    if (fTab.isPreselected()) {
                        return fTab;
                    }

                }
            }
        }
        // TODO: DEFAULT VERHALTEN ANPASSEN
        return null;
    }

    private BundleTabTO getSelectedBundleTab(final DeviceDetailTO deviceDetailTO)
    {
        Map<String, BundleTabTO> tabs = getBundleTabsMap(deviceDetailTO);
        for (Entry<String, BundleTabTO> entry : tabs.entrySet()) {
            BundleTabTO bTab = entry.getValue();
            if (bTab.isPreselected()) {
                return bTab;
            }
        }
        return null;
    }

    private void generateChoosePlanData(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTo, final boolean isMbb)
    {
        // choosePlan
        Map<String, BundleTabTO> bundleTabs = new HashMap<>();
        try {
            bundleTabs = deviceDetailTO.getBundleTabs();
            if (!bundleTabs.isEmpty()) {
                for (BundleTabTO bundleTabTO : bundleTabs.values()) {
                    if (!StringUtils.equalsIgnoreCase(bundleTabTO.getType(), PREPAID_FREQUENCY)) {
                        List<FrequencyTabTO> frequencyTabs = bundleTabTO.getFrequencyTabs();
                        Map<Integer, List<ChoosePlanEntry>> choosePlan =
                                new HashMap<Integer, List<ChoosePlanEntry>>();
                        if (frequencyTabs != null) {
                            for (int j = 0; j < frequencyTabs.size(); j++) {
                                FrequencyTabTO frequencyTabTO = frequencyTabs.get(j);
                                List<FrequencyTabProductTO> frequencyTabProductList =
                                        frequencyTabTO.getFrequencyTabProductList();
                                if (frequencyTabProductList != null) {
                                    for (int k = 0; k < frequencyTabProductList.size(); k++) {
                                        FrequencyTabProductTO frequencyTabProductTO = frequencyTabProductList.get(k);
                                        addChoosePlanEntry(frequencyTabProductTO, deviceDetailListTo, choosePlan, isMbb);
                                    }
                                }

                                else {
                                    LOG.error("frequencyTabProductList is empty");
                                }
                            }
                        } else {
                            LOG.error("frequencyTabs is empty");
                        }
                        deviceDetailListTo.setPostpaidPlans(choosePlan);
                    }
                }
            } else {
                LOG.error("BundleTabs is empty");
            }
        } catch (Exception e) {
            LOG.error("DeviceDetailTo.getBundleTabs throws an exception:" + e.getMessage());
        }
    }



    private void addChoosePlanEntry(FrequencyTabProductTO frequencyTabProductTO,
            DeviceDetailListTO deviceDetailListTo, Map<Integer, List<ChoosePlanEntry>> choosePlan, final boolean isMbb)
    {
        List<ChoosePlanEntry> list = choosePlan
                .get((isMbb && isContains(frequencyTabProductTO.getTermsOfServiceNumber())) ? otherTermName
                        : frequencyTabProductTO.getTermsOfServiceNumber());
        ChoosePlanEntry result = new ChoosePlanEntry();
        if (list == null) {
            list = new LinkedList<ChoosePlanEntry>();
            choosePlan.put((isMbb && isContains(frequencyTabProductTO.getTermsOfServiceNumber())) ? otherTermName
                    : frequencyTabProductTO.getTermsOfServiceNumber(), list);
        }
        list.add(result);
        // populate shared plan details
        result.setSharable(frequencyTabProductTO.isSharable());
        result.setValidDataSharerCount(frequencyTabProductTO.getValidDataSharerCount());
        result.setValidMobileSharerCount(frequencyTabProductTO.getValidMobileSharerCount());
        result.setSharingDescription(frequencyTabProductTO.getSharingDescription());

        result.setChoosePlanEntryid(frequencyTabProductTO.getFrequencyCode());

        for (ClassificationTO classification : frequencyTabProductTO.getClassifications()) {
            if (classification != null && classification.getCode() != null) {
                if (classification.getCode().equalsIgnoreCase("shopgenericpromomessageclassification")) {
                    result.setPromotionalMessages(classification.getFeatureTOList());
                }
            }
        }

        for (EntitlementsTO entry : frequencyTabProductTO.getEntitlementMap().values()) {
            if (StringUtils.equals(result.getIncludedMinutes(), "0")
                    && StringUtils.containsIgnoreCase(entry.getUsageType(), "Voice")) {
                result.setIncludedMinutes(entry.getName());
            } else if (StringUtils.equals(result.getIncludedTexts(), "0")
                    && StringUtils.containsIgnoreCase(entry.getUsageType(), "Text")) {
                result.setIncludedTexts(entry.getQuantity());
            } else if (StringUtils.equals(result.getIncludedData(), "0MB")
                    && StringUtils.isEmpty(entry.getAdditionalInformation())
                    && StringUtils.containsIgnoreCase(entry.getUsageType(), "Data")) {
                if (isMbb && entry.isUnlimited()) {
                    result.setIncludedData("Unlimited*");
                } else {
                    result.setIncludedData(entry.getName());
                }
            } else if (StringUtils.containsIgnoreCase(entry.getUsageType(), "Other")) {
                result.addAdditionalEntitlements(entry.getName(), entry.getAdditionalInformation());
            } else {
                if (entry.isUnlimited()) {
                    result.addAdditionalEntitlements(entry.getName(), "unlimited");
                } else {
                    result.addAdditionalEntitlements(entry.getName(), entry.getQuantity() + entry.getUsageUnitName()
                            + " " + entry.getAdditionalInformation());
                }

            }
        }
        if (isMbb) {
            result.setMonthlyCost(frequencyTabProductTO.getThisBundleProductPrice().getAmountAsString());
        } else {
            result.setMonthlyCost(frequencyTabProductTO.getThisBundleProductPrice().getFormattedValue());
        }
        result.setPhoneCost(frequencyTabProductTO.getOtherBundleProductPrice().getFormattedValue());
        result.setBadge(frequencyTabProductTO.getBadge());


        // Adding Terms Of Service Number to Choose Plan entry
        result.setTermsOfServiceNumber(frequencyTabProductTO.getTermsOfServiceNumber());

        // Adding Credits Description to plan entry
        for (CreditsTO credit : frequencyTabProductTO.getCreditList()) {
            result.getCredits().add(credit.getDescription());
        }

        // Adding Billing Frequency to ChoosePlan Entry
        result.setBillingFrequency(frequencyTabProductTO.getBillingFrequency());

        // Adding Terms Of SErvice Frequency to ChoosePlan Entry
        result.setTermOfServiceFrequency(frequencyTabProductTO.getTermOfServiceFrequency());

        // Adding Terms Of Product Badge to ChoosePlan Entry
        result.setProductBadge(frequencyTabProductTO.getProductBadge());

        for (UsageChargeTO entry : frequencyTabProductTO.getUsageChargeList()) {
            result.addAdditionalCosts(entry.toString());
        }
    }

    /**
     * This method is used to check whether the parameter is there in the list or not list: {12,24}
     * 
     * @param number
     * @return
     */
    protected boolean isContains(int number)
    {
        for (int i : otherTermplans) {
            if (i == number)
                return true;
        }
        return false;
    }

    /**
     * Creates a list of serviceAddOns for the prepaid plan. Also identifies whether Casual rate is
     * enabled or not. Also extracts the prepaid plan from the hybris response so that it can be
     * used in the jsp.
     */
    private void generatePrepaidChoosePlanData(final DeviceDetailTO deviceDetailTO,
            DeviceDetailListTO deviceDetailListTo)
    {
        // choosePlan
        Map<String, BundleTabTO> bundleTabs;
        try {
            bundleTabs = deviceDetailTO.getBundleTabs();
            if (!bundleTabs.isEmpty()) {
                for (BundleTabTO bundleTabTO : bundleTabs.values()) {
                    // only extract the prepaid plan data.
                    if (StringUtils.equalsIgnoreCase(bundleTabTO.getType(), PREPAID_FREQUENCY)) {

                        // TO DO : find out if prepaid has only one frequency tab which is No
                        // contract at the moment
                        List<FrequencyTabTO> frequencyTabs = bundleTabTO.getFrequencyTabs();
                        if (frequencyTabs != null) {
                            HashMap<String, ServiceAddOnProductTO> prepaidChooseServiceAddOn =
                                    new HashMap<String, ServiceAddOnProductTO>();
                            for (int j = 0; j < frequencyTabs.size(); j++) {
                                FrequencyTabTO frequencyTabTO = frequencyTabs.get(j);
                                List<FrequencyTabProductTO> frequencyTabProductList =
                                        frequencyTabTO.getFrequencyTabProductList();
                                if (frequencyTabProductList != null) {
                                    for (int k = 0; k < frequencyTabProductList.size(); k++) {
                                        FrequencyTabProductTO frequencyTabProductTO = frequencyTabProductList.get(k);
                                        // Add the prepaid-service addon data in the prepaid object
                                        HashMap<String, ServiceAddOnProductTO> prepaidServiceAddOn =
                                                addPrepaidChoosePlanEntry(frequencyTabProductTO,
                                                        prepaidChooseServiceAddOn);
                                        deviceDetailListTo.setPrepaidPlans(prepaidServiceAddOn);
                                        // this is hard-coded to pick it up from first entry of
                                        // frequencyTabProductList.
                                        if (k == 0) {
                                            boolean isCasualRateEnabled =
                                                    (frequencyTabProductTO.getMinSelectionCriteria() == 0);
                                        }

                                        // This is also hardcoded to "No Contract".
                                        if (frequencyTabTO.getTermOfServiceFrequencyName()
                                                .equalsIgnoreCase("No Contract")) {
                                            if (StringUtils.isEmpty(frequencyTabProductTO.getFrequencyCode())) {
                                                deviceDetailListTo.setCurrentPrepaidPlan(
                                                        Constants.DEFAULT_PREPAID_SERVICE_PLAINID);
						deviceDetailListTo.setPrepaidServicePlanId(
							Constants.DEFAULT_PREPAID_SERVICE_PLAINID);
                                            } else {
                                                deviceDetailListTo.setCurrentPrepaidPlan(
                                                        frequencyTabProductTO.getFrequencyCode());
						deviceDetailListTo.setPrepaidServicePlanId(
							frequencyTabProductTO.getFrequencyCode());
                                            }
                                            deviceDetailListTo.setPrepaidServicePlanName(
                                                    frequencyTabProductTO.getName());
                                            deviceDetailListTo
                                                    .setPrepaidPlanType(frequencyTabProductTO.getPlanType());

                                            if (frequencyTabProductTO.getOneTimeChargeEntryPrice() != null) {
                                                deviceDetailListTo.setPrepaidServicePlanPrice(StringUtils.isNotEmpty(
                                                        frequencyTabProductTO.getOneTimeChargeEntryPrice()
                                                                .getAmountAsString())
                                                                        ? frequencyTabProductTO
                                                                                .getOneTimeChargeEntryPrice()
                                                                                .getAmountAsString()
                                                                        : "free");
                                            } else if (frequencyTabProductTO.getRecurringChargeEntryPrice() != null) {
                                                deviceDetailListTo.setPrepaidServicePlanPrice(StringUtils.isNotEmpty(
                                                        frequencyTabProductTO.getRecurringChargeEntryPrice()
                                                                .getAmountAsString())
                                                                        ? frequencyTabProductTO
                                                                                .getRecurringChargeEntryPrice()
                                                                                .getAmountAsString()
                                                                        : "free");
                                            } else {
                                                deviceDetailListTo.setPrepaidServicePlanPrice("free");
                                            }
                                        }
                                    }
                                } else {
                                    LOG.error("frequencyTabProductList is empty");
                                }
                            }
                        } else {
                            LOG.error("frequencyTabs is empty");
                        }
                    }
                }
            } else {
                LOG.error("BundleTabs is empty");
            }
        } catch (Exception e) {
            LOG.error("DeviceDetailTo.getBundleTabs throws an exception:" + e.getMessage());
        }
    }

    /**
     * Updates the list with the serviceAddOnTO objects.
     * 
     * @param frequencyTabProductTO
     */
    private HashMap<String, ServiceAddOnProductTO> addPrepaidChoosePlanEntry(
            FrequencyTabProductTO frequencyTabProductTO,
            HashMap<String, ServiceAddOnProductTO> prepaidChooseServiceAddOn)
    {

        Map<String, ServiceAddOnProductTO> serviceAddOnMap = frequencyTabProductTO.getServiceAddOnProductTO();
        if (!serviceAddOnMap.isEmpty()) {
            prepaidChooseServiceAddOn.putAll(serviceAddOnMap);
        }
        return prepaidChooseServiceAddOn;
    }

    /**
     * This returns postpaid entitlement json.
     *
     * @return String postpaid entitlement
     */
    private void getPostpaidEntitlementsJson(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        deviceDetailListTO.setPostpaidEntitlementsJson(getPostpaidEntitlements(deviceDetailTO));
    }

    /**
     * This method return all the entitlements whose image are present in dam.
     * 
     * @return entitlementMap
     */
    private Map<String, EntitlementsTO> getPostpaidEntitlements(final DeviceDetailTO deviceDetailTO)
    {
        Session session = null;
        int count = 0;
        Map<String, EntitlementsTO> entitlementMap = new LinkedHashMap<String, EntitlementsTO>();
        FrequencyTabProductTO postpaidProduct = getValidPostpaidProduct(deviceDetailTO);
        if (null != postpaidProduct) {
            Map<String, EntitlementsTO> productEntitlements = postpaidProduct.getEntitlementMap();
            if (MapUtils.isNotEmpty(productEntitlements)) {
                try {
                    session = adminSessionFactory.createSession();
                    Iterator<Map.Entry<String, EntitlementsTO>> iterator = productEntitlements.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<String, EntitlementsTO> entry = iterator.next();
                        if (entry.getValue() != null
                                && StringUtils.containsIgnoreCase(entry.getValue().getUsageType(), "other")) {
                            if (entitlementService.getEntitlementSizeForPostpaid() >= count) {
                                String path = ENTITLEMENT_DAM_PATH + entry.getValue().getAllowanceId().toLowerCase()
                                        + ".png";
                                Node entitlementImage = entitlementService.getNodeFromSession(session, path);
                                if (entitlementImage != null) {
                                    entitlementMap.put(entry.getKey(), entry.getValue());
                                    count++;
                                }
                            }
                        }
                    }
                } catch (RepositoryException e) {
                    LOG.error("Repository Exception while creating admin session", e);
                } finally {
                    if (session != null) {
                        session.logout();
                    }
                }
            }
        }
        return entitlementMap;
    }

    /**
     * identifies and returns the valid postpaid product(service plan) which is to be shown in the
     * UI under postpaid tab
     * 
     * @return FrequencyTabProductTO- postpaid product
     */
    private FrequencyTabProductTO getValidPostpaidProduct(final DeviceDetailTO deviceDetailTO)
    {
        FrequencyTabProductTO postpaidProduct = null;
        FrequencyTabProductTO preselectedProduct = getPreselectedProduct(deviceDetailTO);
        if (getBundleTabsMap(deviceDetailTO).get("Pay Monthly") != null) {
            if (getBundleTabsMap(deviceDetailTO).get("Pay Monthly").isPreselected() && preselectedProduct != null) {
                postpaidProduct = preselectedProduct;
            } else if (getDefaultServicePlanAvailable(deviceDetailTO)) {
                postpaidProduct = getDefaultPostPaidProduct(deviceDetailTO);
            } else {
                postpaidProduct = getFirstPostpaidProduct(deviceDetailTO);
            }
        }
        return postpaidProduct;
    }

    private FrequencyTabProductTO getPrepaidProduct(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        FrequencyTabProductTO prepaidProduct = null;
        if (null != getBundleTabsMap(deviceDetailTO) && null != getBundleTabsMap(deviceDetailTO).get("Prepaid")
                && null != getBundleTabsMap(deviceDetailTO).get("Prepaid").getFrequencyTabs()) {

            List<FrequencyTabTO> frequencyTabs = getBundleTabsMap(deviceDetailTO).get("Prepaid").getFrequencyTabs();
            for (FrequencyTabTO frequencyTabTO : frequencyTabs) {
                if (!frequencyTabTO.getFrequencyTabProductList().isEmpty()) {
                    prepaidProduct = frequencyTabTO.getFrequencyTabProductList().get(0);
                    deviceDetailListTO.setPrepaidProduct(prepaidProduct);
                }
            }
        }
        return prepaidProduct;
    }

    /**
     * This returns original colour json.
     *
     * @return String original colour json
     */
    private void getOriginalColourJson(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        if (null != deviceDetailTO && null != deviceDetailTO.getProductColour()) {
            deviceDetailListTO.setOriginalProductColour(deviceDetailTO.getProductColour());
        }
    }

    /**
     * This method converts other colour list into json.
     *
     * @return String other colour json
     */
    private void getOtherColourJson(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO,
            SlingHttpServletRequest request)
    {
        List<ProductRefGroupTO.OtherColour> otherColourList = new ArrayList<>();
        List<Target> productRefGroupTargets = getProdReferences(deviceDetailTO, "OTHER_COLOUR");
        if (productRefGroupTargets != null) {
            for (Target productColor : productRefGroupTargets) {
                if (productColor != null) {
                    otherColourList.add(new ProductRefGroupTO.OtherColour(productColor.getProductColour().getHexCode(),
                            productColor.getName(), productColor.getCode(),
                            request.getResourceResolver().map(productColor.getUrl())));
                }
            }
            deviceDetailListTO.setOtherColours(otherColourList);
        }
    }
    private void getOriginalDeviceSize(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        deviceDetailListTO.setOriginalSize(deviceDetailTO.getSize() != null ? deviceDetailTO.getSize() : "");
    }

    /**
     * This method converts other size list into json.
     *
     * @return String other size list json
     */
    private void getOtherSizeJson(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO,
            SlingHttpServletRequest request)
    {
        List<ProductRefGroupTO.OtherSize> otherSizeList = new ArrayList<>();
        List<Target> productRefGroupTargets = getProdReferences(deviceDetailTO, "OTHER_SIZE");
        if (productRefGroupTargets != null) {
            for (Target productSize : productRefGroupTargets) {
                if (productSize != null) {
                    otherSizeList.add(new ProductRefGroupTO.OtherSize(
                            productSize.getCode(), productSize.getSize(),
                            request.getResourceResolver().map(productSize.getUrl())));
                }
            }
            deviceDetailListTO.setOtherSizes(otherSizeList);
        }
    }

    private List<Target> getProdReferences(final DeviceDetailTO deviceDetailTO, final String referenceType)
    {
        if (deviceDetailTO != null && deviceDetailTO.getProductReferenceGroups() != null) {
            List<ProductReferenceGroup> productRefGroupTo = deviceDetailTO.getProductReferenceGroups();
            // return productRefGroupTo.stream().filter(s -> s.getReferenceType() != null &&
            // s.getReferenceType().equals(referencType)).findAny().get().getTargets();
            for (ProductReferenceGroup productRefGroup : productRefGroupTo) {
                if (productRefGroup.getReferenceType() != null && referenceType.equals(productRefGroup.getReferenceType())) {
                    return productRefGroup.getTargets();
                }
            }
        }
        return null;
    }

    /**
     * Selects the pre-selected serviceAddOn from the list of prepaidChooseServiceAddOn
     * 
     * @return
     */
    private ServiceAddOnProductTO getSelectedServiceAddOn(final DeviceDetailListTO deviceDetailListTO)
    {
        if (deviceDetailListTO.getPrepaidPlans() != null) {
            for (Map.Entry<String, ServiceAddOnProductTO> entry : deviceDetailListTO.getPrepaidPlans().entrySet()) {
                ServiceAddOnProductTO serviceAddOnProductTO = entry.getValue();
                if (serviceAddOnProductTO.isPreselected()) {
                    return serviceAddOnProductTO;
                }
            }
        }
        return null;
    }

    /**
     * This method converts prepaid service add on map into json.
     *
     * @return String prepaid service add on map json
     */
    private void getPrepaidChooseServiceAddOnJson(final DeviceDetailListTO deviceDetailListTO)
    {

        deviceDetailListTO.setPrepaidPlans(getPrepaidChooseServiceAddOn(deviceDetailListTO));
    }

    /**
     * Returns a sorted list of serviceAddOns for prepaid plans which are returned by hybris.
     * 
     * @return
     */
    private Map<String, ServiceAddOnProductTO> getPrepaidChooseServiceAddOn(
            final DeviceDetailListTO deviceDetailListTO)
    {
        return getSortedServiceAddOns(deviceDetailListTO);
    }



    /**
     * Sorts the Map of ServiceAddons in Ascending order based on the price of the serviceAddOn (can
     * be recurring charge otherwise one time charge).
     * 
     * @return Map<String,ServiceAddOnProductTO>
     */
    private Map<String, ServiceAddOnProductTO> getSortedServiceAddOns(final DeviceDetailListTO deviceDetailListTO)
    {

        Comparator<Map.Entry<String, ServiceAddOnProductTO>> byMapValues =
                new Comparator<Map.Entry<String, ServiceAddOnProductTO>>() {

                    @Override
                    public int compare(Map.Entry<String, ServiceAddOnProductTO> left,
                            Map.Entry<String, ServiceAddOnProductTO> right)
                    {

                        BigDecimal firstValue = new BigDecimal(0.0);
                        BigDecimal secondValue = new BigDecimal(0.0);

                        if (null != left) {

                            if (null != left.getValue().getRecurringChargeEntryPrice() && null != left
                                    .getValue().getRecurringChargeEntryPrice().getAmount()) {
                                firstValue = left.getValue().getRecurringChargeEntryPrice().getAmount();
                            } else {
                                if (null != left.getValue().getOneTimeChargeEntryPrice() && null != left
                                        .getValue().getOneTimeChargeEntryPrice().getAmount()) {
                                    firstValue = left.getValue().getOneTimeChargeEntryPrice().getAmount();
                                }
                            }
                        }

                        if (null != right) {

                            if (null != right.getValue().getRecurringChargeEntryPrice() && null != right
                                    .getValue().getRecurringChargeEntryPrice().getAmount()) {
                                secondValue = right.getValue().getRecurringChargeEntryPrice().getAmount();
                            } else {
                                if (null != right.getValue().getOneTimeChargeEntryPrice() && null != right
                                        .getValue().getOneTimeChargeEntryPrice().getAmount()) {
                                    secondValue = right.getValue().getOneTimeChargeEntryPrice().getAmount();
                                }
                            }
                        }
                        return firstValue.compareTo(secondValue);
                    }
                };
        LinkedHashMap<String, ServiceAddOnProductTO> sortedMap = new LinkedHashMap<String, ServiceAddOnProductTO>();
        // create a list of map entries
        List<Map.Entry<String, ServiceAddOnProductTO>> serviceAddOnList =
                new ArrayList<Map.Entry<String, ServiceAddOnProductTO>>();

        if (null != deviceDetailListTO.getPrepaidPlans() && !deviceDetailListTO.getPrepaidPlans().isEmpty()) {

            serviceAddOnList.addAll(deviceDetailListTO.getPrepaidPlans().entrySet());

            // sort the collection
            Collections.sort(serviceAddOnList, byMapValues);
            Collections.reverse(serviceAddOnList);
            for (Map.Entry<String, ServiceAddOnProductTO> entry : serviceAddOnList) {
                sortedMap.put(entry.getKey(), entry.getValue());
            }

        } else {
            return deviceDetailListTO.getPrepaidPlans();
        }
        return sortedMap;
    }

    private void getDefaultPrepaidPlanAddOnAvailable(final DeviceDetailListTO deviceDetailListTO)
    {

        if (deviceDetailListTO.getPrepaidPlans() != null) {
            for (Map.Entry<String, ServiceAddOnProductTO> entry : deviceDetailListTO.getPrepaidPlans().entrySet()) {
                ServiceAddOnProductTO serviceAddOnProductTO = entry.getValue();
                if (serviceAddOnProductTO.isPreselected()) {
                    deviceDetailListTO.setDefaultAddOnSelected(true);
                    deviceDetailListTO.setCompatibleAddOn(serviceAddOnProductTO.getCode());
                    deviceDetailListTO.setCompatibleAddOnTemplateId(serviceAddOnProductTO.getAddOnBundleTemplateId());
                }
            }
        }
    }



    private void getCreditRuleJson(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        Map<String, List<Rule>> creditRuleMap = new LinkedHashMap<String, List<Rule>>();
        if (getSelectedBundleTab(deviceDetailTO) != null
                && getSelectedBundleTab(deviceDetailTO).getCreditComponent() != null
                && CollectionUtils.isNotEmpty(
                        getSelectedBundleTab(deviceDetailTO).getCreditComponent().getProducts())) {
            for (ProductTO product : getSelectedBundleTab(deviceDetailTO).getCreditComponent().getProducts()) {
                if (product.getCreditRules() != null) {
                    creditRuleMap.put(getSelectedBundleTab(deviceDetailTO).getCreditComponent().getId() + "-"
                            + product.getCode(), product.getCreditRules());
                }
            }
        }
        deviceDetailListTO.setCreditRules(creditRuleMap);
    }

    private void getGiftRuleJson(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        Map<String, List<Rule>> giftRuleMap = new LinkedHashMap<String, List<Rule>>();
        if (getSelectedBundleTab(deviceDetailTO) != null
                && getSelectedBundleTab(deviceDetailTO).getGiftComponent() != null
                && CollectionUtils.isNotEmpty(
                        getSelectedBundleTab(deviceDetailTO).getGiftComponent().getProducts())) {
            for (ProductTO product : getSelectedBundleTab(deviceDetailTO).getGiftComponent().getProducts()) {
                if (product.getGiftRules() != null) {
                    giftRuleMap.put(getSelectedBundleTab(deviceDetailTO).getGiftComponent().getId() + "-"
                            + product.getCode(), product.getGiftRules());
                }
            }
        }
        deviceDetailListTO.setGiftRules(giftRuleMap);
    }

    private void getCreditProductsAsJson(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        List<ProductTO> products = new ArrayList<ProductTO>();
        if (getSelectedBundleTab(deviceDetailTO) != null
                && getSelectedBundleTab(deviceDetailTO).getCreditComponent() != null) {
            products = getSelectedBundleTab(deviceDetailTO).getCreditComponent().getProducts();
        }
        deviceDetailListTO.setCreditProduct(products);
    }

    private void getGiftProductsAsJson(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        List<ProductTO> products = new ArrayList<ProductTO>();
        if (this.getSelectedBundleTab(deviceDetailTO) != null
                && this.getSelectedBundleTab(deviceDetailTO).getGiftComponent() != null) {
            products = this.getSelectedBundleTab(deviceDetailTO).getGiftComponent().getProducts();
        }
        deviceDetailListTO.setGiftProducts(products);
    }

    private boolean isClassificationAvailable(final DeviceDetailTO deviceDetailTO) {
	return !getClassificationMap(deviceDetailTO).isEmpty();
    }

    private Map<String, List<ClassificationTO>> getClassificationMap(final DeviceDetailTO deviceDetailTO)
    {
        if (deviceDetailTO != null && deviceDetailTO.getClassifications() != null
                && !deviceDetailTO.getClassifications().isEmpty()) {
            return deviceDetailTO.getClassifications();
        } else {
            return Collections.emptyMap();
        }
    }

    private void getClassification(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        LOG.info("Start-getClassification");
        if (isClassificationAvailable(deviceDetailTO)) {
            Map<String, List<ClassificationTO>> clasificationMap = getClassificationMap(deviceDetailTO);
            Set<String> keys = clasificationMap.keySet();
            for (String key : keys) {
                List<ClassificationTO> Valuelist = clasificationMap.get(key);
                for (ClassificationTO classTo : Valuelist) {
                    if (StringUtils.isNotBlank(classTo.getName())) {

                        if (StringUtils.isNotBlank(classTo.getName())
                                && classTo.getName().toLowerCase().contains("camera")) {
                            deviceDetailListTO.setClassificationCamera(classTo);
                        } else if (StringUtils.isNotBlank(classTo.getName())
                                && classTo.getName().toLowerCase().contains("battery")) {
                            deviceDetailListTO.setClassificationBattery(classTo);
                        } else if (StringUtils.isNotBlank(classTo.getName())
                                && classTo.getName().toLowerCase().contains("screen")) {
                            deviceDetailListTO.setClassificationScreen(classTo);
                        }

                    }
                }
            }
        }

        LOG.info("End-getClassification");
    }

    /**
     * This method filters the postpaid plans and service addons for SS change plan journey. If the
     * user has selected a postpaid plan and again coming to device detail page by selecting buy
     * with device then he should be shown with only the plan he has selected before.
     */
    private void filterPlansForChangePlanJourney(final DeviceDetailListTO deviceDetailListTO,
            SlingHttpServletRequest request)
    {
        final String journeyType = request.getParameter(URLParamConstants.JOURNEY_TYPE);
        final CPWithDeviceData changePlanData = changePlanWithDeviceService.getCPWithDeviceDataFromSession(request);
        if (Constants.CP_WITH_DEVICE.equals(journeyType) && changePlanData != null) {
            // clearing all service addons as we should not show prepaid tab
            if (changePlanWithDeviceService.isFilterServiceAddons() && null != deviceDetailListTO.getPrepaidPlans()) {
                deviceDetailListTO.getPrepaidPlans().clear();
            }
            if (!Constants.PRE_TO_POST.equals(changePlanData.getSubJourney())
                    && changePlanWithDeviceService.isFilterPostpaidPlans()) {
                // need to remove all plans except the selected postpaid plan only for postpaid to
                // postpaid change plan journey and not for prepaid to postpaid change plan
                final String servicePlanId = changePlanData.getChosenPlanId();
                boolean servicePlanFound = false;
                if (deviceDetailListTO.getPostpaidPlans() != null && StringUtils.isNotEmpty(servicePlanId)) {
                    Iterator<Entry<Integer, List<ChoosePlanEntry>>> choosePlanMapiterator =
                            deviceDetailListTO.getPostpaidPlans().entrySet().iterator();
                    while (choosePlanMapiterator.hasNext()) {
                        servicePlanFound = false;
                        Map.Entry<Integer, List<ChoosePlanEntry>> entry = choosePlanMapiterator.next();
                        List<ChoosePlanEntry> choosePlanEntries = entry.getValue();
                        if (CollectionUtils.isNotEmpty(choosePlanEntries)) {
                            Iterator<ChoosePlanEntry> planListIterator = choosePlanEntries.iterator();
                            while (planListIterator.hasNext()) {
                                ChoosePlanEntry choosePlanEntry = planListIterator.next();
                                if (servicePlanId.equals(choosePlanEntry.getChoosePlanEntryid())) {
                                    servicePlanFound = true;
                                } else {
                                    planListIterator.remove();
                                }
                            }
                        }
                        if (!servicePlanFound) {
                            choosePlanMapiterator.remove();
                        }
                    }
                }
            }
        }
    }



    public String getDeviceDetailLegaltermsPostpaidPhones()
    {
        return config.getDeviceDetailLegaltermsPostpaidPhones();
    }

    public String getDeviceDetailLegaltermsPrepaidPhones()
    {
        return config.getDeviceDetailLegaltermsPrepaidPhones();
    }


    private PriceInfoTNZ getRecommendedRetailPriceInfoTNZ(final DeviceDetailTO deviceDetailTO)
    {
        return deviceDetailTO != null ? deviceDetailTO.getPriceInfoTNZ() : null;
    }

    private void setPrepaidPriceAndCurrency(final DeviceDetailListTO deviceDetailListTO)
    {
        if (deviceDetailListTO.isDefaultAddOnSelected()) {
            deviceDetailListTO.setPrepaidDeviceCurrencySymbol(
                    deviceDetailListTO.getSelectedServiceAddOn().getOtherBundleProductPrice().getSymbol());
            deviceDetailListTO.setPrepaidDevicePrice(deviceDetailListTO.getSelectedServiceAddOn()
                    .getOtherBundleProductPrice().getAmountAsString());
        } else {
            deviceDetailListTO.setPrepaidDeviceCurrencySymbol(
                    deviceDetailListTO.getPrepaidProduct().getOtherBundleProductPrice().getSymbol());
            deviceDetailListTO.setPrepaidDevicePrice(
                    deviceDetailListTO.getPrepaidProduct().getOtherBundleProductPrice().getAmountAsString());
        }
    }

    private void setServicePlanid(final DeviceDetailTO deviceDetailTO, final DeviceDetailListTO deviceDetailListTO)
    {
        if (getBundleTabsMap(deviceDetailTO).get(POSTPAID_BUNDLE).isPreselected()) {
            deviceDetailListTO.setCurrentPostpaidPlan(getPreselectedProduct(deviceDetailTO).getFrequencyCode());
        } else {
            if (getDefaultServicePlanAvailable(deviceDetailTO)) {
                deviceDetailListTO
                        .setCurrentPostpaidPlan(getDefaultPostPaidProduct(deviceDetailTO).getFrequencyCode());
            } else {
                deviceDetailListTO.setCurrentPostpaidPlan(getFirstPostpaidProduct(deviceDetailTO).getFrequencyCode());
            }
        }
    }

    private void setShortDescription(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO, Product baseProduct)
    {
        if (deviceDetailTO != null) {
            deviceDetailListTO.setShortDescription(deviceDetailTO.getShortDescription());
        } else if (baseProduct.getProperty("shortDescription", String.class) != null) {
            deviceDetailListTO.setShortDescription(baseProduct.getProperty("shortDescription", String.class));
        }
    }

    private void getOutOfStockMessages(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        List<StockInfo> stockInfoList = getStockInfoList(deviceDetailTO, deviceDetailListTO);
        if (stockInfoList != null) {
            for (StockInfo stockInfo : stockInfoList) {
                if (OUT_OF_STOCK.equals(stockInfo.getCode())) {
                    deviceDetailListTO.setOutOfStock(true);
                    if (StringUtils.isNotEmpty(stockInfo.getNextDeliveryDate())) {
                        deviceDetailListTO.setOutOfStockMessage(
                                OUT_OF_STOCK_MESSAGE + " " + stockInfo.getNextDeliveryDate());
                    } else {
                        deviceDetailListTO.setOutOfStockMessage(OUT_OF_STOCK_MESSAGE);

                    }
                }
            }
        }
    }

    private void getPreOrderForDevice(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        List<StockInfo> stockInfoList = getStockInfoList(deviceDetailTO, deviceDetailListTO);

        if (stockInfoList != null) {
            for (StockInfo stockInfo : stockInfoList) {
                deviceDetailListTO.setIsPreorderable(stockInfo.getIsPreorderable());
            }
        }
    }

    private List<StockInfo> getStockInfoList(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        if (deviceDetailTO != null && deviceDetailTO.getStockinfoList() != null
                && !deviceDetailTO.getStockinfoList().isEmpty()) {
            deviceDetailListTO.setStockInfoList(deviceDetailTO.getStockinfoList());
            return deviceDetailTO.getStockinfoList();
        } else {
            return null;
        }
    }

    private boolean isEligibleForDeferredPayment(final DeviceDetailTO deviceDetailTO)
    {
        if (deviceDetailTO != null && deviceDetailTO.getEligibleForDeferredPayment() != null) {
            return deviceDetailTO.getEligibleForDeferredPayment();
        } else {
            return false;
        }
    }

    private Boolean getIsCustomerIdentified(SlingHttpServletRequest request, HybrisRequestHeader hybrisRequestHeader,
            SlingHttpServletResponse response)
    {
        Boolean isCustomerIdentified = false;
        String customerNumber = null;
        if (null != shopLoginService) {
            customerNumber = shopLoginService.checkCustomerNumberInSessionState(request);
        }
        if (StringUtils.isEmpty(customerNumber) && null != addToCartService) {
            LOG.info("if customer number is empty. trying to identify customer from cart");
            isCustomerIdentified = addToCartService.identifyCustomerFromCart(hybrisRequestHeader, request, response);
        }
        Identity identity = shopLoginService.getIdentityFromSessionState(request);
        if (identity != null && StringUtils.isNotEmpty(identity.getUid())) {
            isCustomerIdentified = true;
        }
        return isCustomerIdentified;
    }

    private void generateDeferredPaymentData(final DeviceDetailTO deviceDetailTO,
            final DeviceDetailListTO deviceDetailListTO)
    {
        HashMap<String, List<DeferredPaymentTO>> defPaymentData = new HashMap<String, List<DeferredPaymentTO>>();
        Map<String, BundleTabTO> bundleTabs;
        try {
            bundleTabs = deviceDetailTO.getBundleTabs();
            if (!bundleTabs.isEmpty()) {
                for (BundleTabTO bundleTabTO : bundleTabs.values()) {
                    if (!StringUtils.equalsIgnoreCase(bundleTabTO.getType(), PREPAID_FREQUENCY)) {
                        List<FrequencyTabTO> frequencyTabs = bundleTabTO.getFrequencyTabs();
                        if (frequencyTabs != null) {
                            for (int j = 0; j < frequencyTabs.size(); j++) {
                                FrequencyTabTO frequencyTabTO = frequencyTabs.get(j);
                                List<FrequencyTabProductTO> frequencyTabProductList =
                                        frequencyTabTO.getFrequencyTabProductList();
                                if (frequencyTabProductList != null) {
                                    for (int k = 0; k < frequencyTabProductList.size(); k++) {
                                        FrequencyTabProductTO frequencyTabProductTO = frequencyTabProductList.get(k);
                                        List<DeferredPaymentTO> deferPaymentList = new ArrayList<>();
                                        for (DeferredPaymentTO deferredPaym : frequencyTabProductTO
                                                .getDeferPaymentList()) {
                                            deferredPaym.setEnforcedDeferredPayment(frequencyTabProductTO.getEnforcedDeferredPayment());
                                            deferPaymentList.add(deferredPaym);
                                            defPaymentData.put(frequencyTabProductTO.getCode(), deferPaymentList);
                                        }
                                    }
                                }
                            }

                        }

                    }

                }
            }
            deviceDetailListTO.setDeferPaymentList(defPaymentData);
        } catch (Exception e) {
            LOG.error("DeviceDetailTo.getBundleTabs throws an exception:" + e.getMessage());
        }
    }

    /**
     * The method returns all the child nodes paths under a root node.
     *
     * @param nodePath - current root node path under from where the child nodes are to be fetched
     * @param propertyName - name of the property for which we require querying the repository
     * @param session - request session
     * 
     * 
     */

    private List<String> getReferencePath(final String nodePath, final String deviceId, final Session session)
    {
        final Map<String, String> queryMap = new HashMap<>();
        String referenceNodePath = StringUtils.EMPTY;
        List<String> images = new ArrayList<>();

        try {
            queryMap.put("type", "cq:Page");
            queryMap.put("path", nodePath);
            queryMap.put("property", "jcr:content/cq:productMaster");
            queryMap.put("property.value", "/%" + deviceId);
            queryMap.put("property.operation", "like");


            final Query query = builder.createQuery(PredicateGroup.create(queryMap), session);
            final SearchResult searchResult = query.getResult();

            for (final Hit hit : searchResult.getHits()) {
                referenceNodePath = hit.getPath();
                images = getImaageReferences(referenceNodePath, session);
                break;
            }
        } catch (final RepositoryException exception) {
            LOG.error("An Exception has occured when getting component node under a page path specified: ", exception);
        }
        return images;
    }

    private List<String> getImaageReferences(final String pathImage, final Session session)
    {

        final Map<String, String> queryMap = new HashMap<>();
        queryMap.put("type", "nt:unstructured");
        queryMap.put("path", pathImage);
        queryMap.put("1_property", "sling:resourceType");
        queryMap.put("1_property.operation", "like");
        queryMap.put("1_property.value", "tnz/components/shop/imagegalleryitem");
        queryMap.put("p.limit", "-1");

        final Query query = builder.createQuery(PredicateGroup.create(queryMap), session);
        final SearchResult searchResult = query.getResult();
        List<String> ImagePaths = new ArrayList<>();
        for (final Hit hit : searchResult.getHits()) {
            try {
                ImagePaths.add(hit.getNode().getProperty("fileReference").getString());
            } catch (ValueFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (PathNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return ImagePaths;
    }

    private CommerceService setCommerceService(final SlingHttpServletRequest request)
    {
        CommerceService commerceService = request.getResource().adaptTo(CommerceService.class);

        return commerceService;
    }

    private HybrisSession setCommerceSession(final SlingHttpServletRequest request, final SlingHttpServletResponse response,
            CommerceService commerceService)
    {
        HybrisSession hybrisSession = null;
        try {
            hybrisSession = (HybrisSession) commerceService.login(request, response);
        } catch (CommerceException e) {
            LOG.error("ERROR when setting CommerceSession: " + e);
        }
        return hybrisSession;
    }

    private boolean isMbb(final SlingHttpServletRequest request)
    {
        return BooleanUtils.toBoolean(request.getParameter("data"));
    }
}
