/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2011 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
package nz.co.spark.commerce.hybris.connection.cmd;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nz.co.spark.commerce.hybris.connection.AbstractHybrisCommand;
import nz.co.tnz.base.services.HybrisRequestHeader;

/**
 *
 */
public class GetProductCommand extends AbstractHybrisCommand {
	
	private static final Logger LOG = LoggerFactory.getLogger(GetProductCommand.class);
	
    public GetProductCommand(final String uri, final String lang, boolean variant, final Set<String> attributes) {
        super();
        
        this.addHeader("Content-Type", "application/json; charset=UTF-8");
        this.addHeader("Accept", "application/json");
        
        setPath(uri);
        LOG.debug("TNZ-URI (GetProductCommand): " + uri);
        
        if(attributes != null) {
            addParameter("options", StringUtils.join(attributes, ","));
        }
        addParameter("lang", lang);
    }

    public GetProductCommand(final String uri, final String lang, boolean variant, final Set<String> attributes,
                    String servicePlanId, String serviceAddOn,List<String> bundleTemplateIds, final HybrisRequestHeader hybrisRequestHeader) {
        super();
        
        this.addHeader("Content-Type", "application/json; charset=UTF-8");
        this.addHeader("Accept", "application/json");
        

        setPath(uri);
        LOG.debug("TNZ-URI (GetProductCommand) with servicePlanId: " + uri);

        if (attributes != null) {
            addParameter("options", StringUtils.join(attributes, ","));
        }
        addParameter("lang", lang);
        addParameter("servicePlanId", servicePlanId);
        if(StringUtils.isNotEmpty(serviceAddOn)){
        	addParameter("serviceAddOn", serviceAddOn);
        }
        if (!CollectionUtils.isEmpty(bundleTemplateIds)) {
            addParameter("bundleTemplateIds", StringUtils.join(bundleTemplateIds, ","));
        }

        if (hybrisRequestHeader != null) {
            addHeader(HEADER_CUSTOMER_REQUESTED_URL, hybrisRequestHeader.getRequestURL());
        }
    }

}
