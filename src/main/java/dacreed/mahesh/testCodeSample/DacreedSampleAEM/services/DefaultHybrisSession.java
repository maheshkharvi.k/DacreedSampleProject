/*************************************************************************
 *
 * ADOBE CONFIDENTIAL __________________
 *
 * Copyright 2011 Adobe Systems Incorporated All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of Adobe Systems
 * Incorporated and its suppliers, if any. The intellectual and technical concepts contained herein
 * are proprietary to Adobe Systems Incorporated and its suppliers and are protected by trade secret
 * or copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from Adobe Systems Incorporated.
 **************************************************************************/

package nz.co.spark.commerce.hybris.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.PaymentMethod;
import com.adobe.cq.commerce.api.PlacedOrder;
import com.adobe.cq.commerce.api.PlacedOrderResult;
import com.adobe.cq.commerce.api.PriceInfo;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.api.ShippingMethod;
import com.adobe.cq.commerce.api.promotion.Promotion;
import com.adobe.cq.commerce.api.promotion.PromotionInfo;
import com.adobe.cq.commerce.api.promotion.Voucher;
import com.adobe.cq.commerce.api.promotion.VoucherInfo;
import com.adobe.cq.commerce.api.smartlist.SmartListManager;
import com.adobe.cq.commerce.common.PriceFilter;
import com.day.cq.wcm.api.PageManager;

import nz.co.spark.api.dto.AddToCartParamsDTO;
import nz.co.spark.base.services.transferobjects.AddToCartItems;
import nz.co.spark.commerce.hybris.HybrisConstants;
import nz.co.spark.commerce.hybris.HybrisSession;
import nz.co.spark.commerce.hybris.HybrisUtil;
import nz.co.spark.commerce.hybris.SessionInfo;
import nz.co.spark.commerce.hybris.api.HybrisService;
import nz.co.spark.commerce.hybris.connection.AbstractHybrisCommand;
import nz.co.spark.commerce.hybris.connection.CommandResult;
import nz.co.spark.commerce.hybris.connection.HybrisAuthenticationHandler;
import nz.co.spark.commerce.hybris.connection.HybrisCommand;
import nz.co.spark.commerce.hybris.connection.HybrisConnection;
import nz.co.spark.commerce.hybris.connection.cmd.AddAddressCommand;
import nz.co.spark.commerce.hybris.connection.cmd.AddBroadbandCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.cmd.AddCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.cmd.AddPaymentInfoCommand;
import nz.co.spark.commerce.hybris.connection.cmd.AuthenticateCustomerInfoCommand;
import nz.co.spark.commerce.hybris.connection.cmd.AuthorizePaymentCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CartMainCommand;
import nz.co.spark.commerce.hybris.connection.cmd.ChangePlanEligibilityCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CheckoutCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CheckoutCreditCardResultCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CheckoutReviewCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CheckoutUpdateCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CheckoutValidateCommand;
import nz.co.spark.commerce.hybris.connection.cmd.CustomerPostalCodeCommand;
import nz.co.spark.commerce.hybris.connection.cmd.DeleteCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.cmd.DeletePassportEntryCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GenericHybrisCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetAccessoryAdvancedFilterCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetAccessoryListingCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetBundleConfigurationCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetCartCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetCheckoutAccountOptionsAccount;
import nz.co.spark.commerce.hybris.connection.cmd.GetCheckoutConfirmationCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetCheckoutDeliveryOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetCheckoutPaymentOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetCheckoutSetupOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetChoosePlanCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetDeviceListingCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetMiniCartCommand;
import nz.co.spark.commerce.hybris.connection.cmd.GetProductCommand;
import nz.co.spark.commerce.hybris.connection.cmd.LogoutCommand;
import nz.co.spark.commerce.hybris.connection.cmd.MobileRetrieveInfoCommand;
import nz.co.spark.commerce.hybris.connection.cmd.ModifyCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.cmd.MoveOrUpgradeBroadbandCommand;
import nz.co.spark.commerce.hybris.connection.cmd.NumberAvailabilityCommand;
import nz.co.spark.commerce.hybris.connection.cmd.ProductAvailabilityCommand;
import nz.co.spark.commerce.hybris.connection.cmd.RedeemVoucherCommand;
import nz.co.spark.commerce.hybris.connection.cmd.RegisterWanterCommand;
import nz.co.spark.commerce.hybris.connection.cmd.ReplaceVoucherCommand;
import nz.co.spark.commerce.hybris.connection.cmd.RetrieveMobileNumberCommand;
import nz.co.spark.commerce.hybris.connection.cmd.RetrieveNumberCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetBundleConfigurationCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetCheckoutAccountOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetCheckoutDeliveryOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetCheckoutYourSetupOptionsCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetDeliveryAddressCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SetDeliveryModeCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SmsCodeGenerationCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SmsCodeValidationCommand;
import nz.co.spark.commerce.hybris.connection.cmd.SubmitCheckoutOrderCommand;
import nz.co.spark.commerce.hybris.connection.cmd.UpdateAddressCommand;
import nz.co.spark.commerce.hybris.connection.cmd.UpdateChosenAccountCommand;
import nz.co.spark.commerce.hybris.connection.cmd.UpdatePaymentAddressCommand;
import nz.co.spark.commerce.hybris.connection.cmd.UpdatePaymentInfoCommand;
import nz.co.spark.commerce.hybris.connection.cmd.UploadPassportCommand;
import nz.co.spark.commerce.hybris.connection.tnz.AddTnzCTCCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.tnz.AddTnzCartEntryCommand;
import nz.co.spark.commerce.hybris.connection.tnz.AddTnzPlanCartEntryCommand;
import nz.co.spark.commerce.hybris.exception.SparkGenericHybrisException;
import nz.co.spark.commerce.hybris.impl.BasicAuthHandler;
import nz.co.spark.commerce.hybris.importer.HybrisResponseParser;
import nz.co.spark.commerce.hybris.importer.XMLEventReaderUtil;
import nz.co.spark.commerce.hybris.to.addtocart.AddToCartTO;
import nz.co.spark.commerce.hybris.to.checkout.Form;
import nz.co.spark.commerce.hybris.to.checkout.ObjectFactory;
import nz.co.spark.commerce.hybris.to.checkout.Page;
import nz.co.spark.commerce.hybris.to.devicelisting.ChangePlanTO;
import nz.co.spark.commerce.hybris.xpathparser.addtocart.AddToCartXPathParser;
import nz.co.tnz.base.core.utils.Constants;
import nz.co.tnz.base.core.utils.ServiceUtil;
import nz.co.tnz.base.services.AccessoryListCacheService;
import nz.co.tnz.base.services.FilterOptionsCacheService;
import nz.co.tnz.base.services.HybrisRequestHeader;
import nz.co.tnz.base.services.JaxbBasedMarshallerCacheService;
import nz.co.tnz.base.services.MorePricingOptionsCacheService;
import nz.co.tnz.base.services.TnzCacheService;

/**
 * <code>DefaultHybrisSession</code>...
 */
public class DefaultHybrisSession implements HybrisSession {

    // After discussion with peter: There is only one Store which has real cart functionality. In
    // Drop 1 is only used personalshop.
    public static final String DEFAULT_BASE_STORE = "personalshop";
    public static final String BASE_STORE="baseStore";

    public static final String MOCK_PARAMETER_NAME = "mock";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultHybrisSession.class.getName());

    private final HybrisConfiguration config;
    private final HybrisConnection connection;
    private final HybrisService hybrisService;
    private final HybrisResponseParser parser;

    // request session information
    private final SlingHttpServletRequest request;
    private final SessionInfo sessionInfo;
    // local shopping cart cache
    private boolean cartCacheDirty = true;
    private ValueMap cartValues;
    private final List<CartEntry> cartEntries = new ArrayList<>();
    private List<PriceInfo> prices;

    private Locale userLocale = null;

    private TnzCacheService cacheService;

    private JaxbBasedMarshallerCacheService jaxbBasedMarshallerCacheService;


    private static final String TRADEUP_BUNDLE_ID = "tradeUpBundleId";


    public DefaultHybrisSession(final HybrisConfiguration config, final HybrisService hybrisService, final SlingHttpServletRequest request,
            final SessionInfo sessionInfo) {
        this.config = config;
        this.connection = config.connection;
        this.hybrisService = hybrisService;
        this.parser = config.parser;
        this.request = request;
        this.sessionInfo = sessionInfo;
        this.cacheService = ServiceUtil.getService(TnzCacheService.class);
        this.jaxbBasedMarshallerCacheService = ServiceUtil.getService(JaxbBasedMarshallerCacheService.class);
    }

    @Override
    public SessionInfo getSessionInfo() {
        return this.sessionInfo;
    }

    @Override
    public CommerceService getCommerceService() {
        return this.hybrisService;
    }

    /**
     * @Deprecated This class should not be used for TNZ, because TNZ has more than one shop.
     *             <ul>
     *             <li>personalshop</li>
     *             <li>businessshop</li>
     *             </ul>
     *             This method can only use ONE, which is configured inside Apache Felix
     */
    @Override
    public String getBaseStore() {
        return this.config.baseStore;
    }

    public void logout(final String pBaseStore) throws CommerceException {
        try {
            this.connection.execute(new LogoutCommand(), pBaseStore, this).release();
        } catch (final CommerceException x) {
            this.hybrisService.checkConnectException(x, true);
        }

        this.sessionInfo.setCookieInfo(null);
    }

    @Override
    public void setUserLocale(final Locale locale) {
        this.userLocale = locale;
    }

    @Override
    public Locale getUserLocale() {
        return this.userLocale;
    }

    /**********************************************************************
     * TNZ specific Stuff
     **********************************************************************/
    @Override
    public List<JSONObject> retrieveDeviceDetailTO(final Product product, final Session jcrSession, final String baseStore,
            final HybrisRequestHeader hybrisRequestHeader)
    {

        JSONObject deviceDetailTO = new JSONObject();
        List<JSONObject> jsonObjectList = new ArrayList<>();

        final Resource resource = product.adaptTo(Resource.class);
        final String uri = HybrisUtil.createProductUri(resource, this.config.productUriTemplateDeviceDetail);
        final boolean variant = HybrisUtil.isVariant(resource);
        final String servicePlanId = getRequestParam("servicePlanId", StringUtils.EMPTY);
        final String compatibileServiceAddon = getRequestParam(Constants.KEY_COMPATIBLESERVICEADDON, StringUtils.EMPTY);

        final List<String> bundleTemplateIds = new ArrayList<>();
        final String tradeUpBundleId = getRequestParam(TRADEUP_BUNDLE_ID, null);
        if (StringUtils.isNotEmpty(tradeUpBundleId)) {
            bundleTemplateIds.add(tradeUpBundleId);
        }

        this.hybrisService.getContext().put(MOCK_PARAMETER_NAME, hybrisRequestHeader.getMsisdn());

        final HybrisConnection.ConnectionOptions opts = new HybrisConnection.ConnectionOptions(baseStore, this, this.hybrisService.getContext());
        CommandResult result = null;

        try {
            final GetProductCommand productCommand = new GetProductCommand(uri, this.hybrisService.getLanguage(), variant,
                    this.config.parser.getConfiguredProductAttributes(variant), servicePlanId, compatibileServiceAddon, bundleTemplateIds, hybrisRequestHeader);

            final HttpMethod httpRequest = this.connection.getRequestMethod(productCommand, baseStore, null);
            final String deviceDetailKey = httpRequest.getPath() + httpRequest.getQueryString();

            if (null != cacheService) {
                if (!this.cacheService.isCached(deviceDetailKey)) {
                    result = this.connection.execute(productCommand, opts);
                    if (result.success()) {
                        try {
                            JSONArray jsonArray = new JSONArray(result.getResponseBodyAsString());
                            if (jsonArray != null) {
                                for (int n = 0; n < jsonArray.length(); n++) {
                                    JSONObject object = jsonArray.getJSONObject(n);
                                    jsonObjectList.add(object);
                                }
                                deviceDetailTO.put("products", jsonArray);
                                this.cacheService.putElementList(deviceDetailKey, jsonObjectList);
                            }
                        } catch (Exception e) {
                            LOG.error("When using xmlInputStream: ", e);
                        }

                    }

                } else {
                    jsonObjectList = this.cacheService.getElement(deviceDetailKey);
                }
            } else {
                LOG.error("cannot retrieve cacheService from ServiceUtil");
                return null;
            }
            return jsonObjectList;
        } catch (final CommerceException | FactoryConfigurationError | UnsupportedEncodingException e) {
            LOG.error("DEVICE_DETAIL: retrieveDeviceDetailTO ", e);
            return null;
        }
    }

    