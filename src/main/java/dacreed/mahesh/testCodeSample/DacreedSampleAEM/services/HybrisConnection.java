/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2011 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
package nz.co.spark.commerce.hybris.connection;

import nz.co.spark.commerce.hybris.HybrisSession;
import nz.co.spark.commerce.hybris.impl.BasicAuthHandler;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * <code>HybrisConnection</code> is a service interface
 * for providing connectivity and request/response processing
 * to a remote Hybris server.
 */
public interface HybrisConnection {
    
    public String getServerUrl();

    public CommandResult execute(HybrisCommand command, ConnectionOptions options) throws CommerceException;

    public CommandResult execute(HybrisCommand command, String baseStore) throws CommerceException;

    public CommandResult execute(HybrisCommand command, String baseStore, CommerceSession session) throws CommerceException;

    public CommandResult execute(HybrisCommand command, String username, String password, String baseStore) throws CommerceException;

    public HttpClient getConfiguredHttpClient();
    
    public HttpMethod getRequestMethod(final HybrisCommand command, String baseStore, final Map<String, Object> context) throws UnsupportedEncodingException ;
    
    /**
     * Provides various options to services implementing the
     * {@link HybrisConnection} interface, such as the base store
     * and session.
     */
    public class ConnectionOptions {

        private String baseStore;
        private Map<String, Object> context;
        private Map<String, String> sessionData;
        private HybrisSession session;

        public ConnectionOptions(final String baseStore, final Map<String, Object> context) {
            this.baseStore = baseStore;
            this.context = context;
            this.session = null;
            this.sessionData = null;
        }

        public ConnectionOptions(final String baseStore, final HybrisSession session, final Map<String, Object> context) {
            this(baseStore, context);
            this.session = session;
            if (session != null && session.getSessionInfo() != null) {
                this.sessionData = session.getSessionInfo().getCookieInfo();
            } else {
                this.sessionData = null;
            }
        }

        public ConnectionOptions(final String baseStore, final HybrisSession session, final Map<String, Object> context,
                        final boolean shouldAbandonCart) {
            this(baseStore, context);
            this.session = session;
            // get the session cookie info, only if the cart is not to be abandoned and a session
            // already exists
            if (session != null && session.getSessionInfo() != null) {
                this.sessionData = session.getSessionInfo().getCookieInfo();
                if(this.sessionData != null && shouldAbandonCart) {
                    this.sessionData.put(BasicAuthHandler.KEY_SESSIONID, null);
                }
            } else {
                this.sessionData = null;
            }
        }
        
        public ConnectionOptions(final String baseStore, final Map<String, String> sessionData, final Map<String, Object> context) {
            this(baseStore, context);
            this.sessionData = sessionData;
        }

        public String getBaseStore() {
            return baseStore;
        }

        public Map<String, Object> getContext() {
            return context;
        }

        public HybrisSession getSession() {
            return session;
        }

        public Map<String, String> getSessionData() {
            return sessionData;
        }
        
       
        
    }
}
