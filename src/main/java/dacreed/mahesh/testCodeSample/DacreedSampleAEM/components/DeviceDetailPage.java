package nz.co.tnz.components.pages;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceService;
import com.google.gson.Gson;

import nz.co.spark.commerce.hybris.HybrisSession;
import nz.co.spark.commerce.hybris.to.ClassificationTO;
import nz.co.spark.commerce.hybris.to.DeferredPaymentTO;
import nz.co.spark.commerce.hybris.to.DeviceDetailListTO;
import nz.co.spark.commerce.hybris.to.SeoDataTO;
import nz.co.spark.commerce.hybris.to.SeoOffersTO;
import nz.co.spark.commerce.hybris.to.checkout.Cart.CartEntries.CartEntry;
import nz.co.spark.commerce.hybris.to.checkout.Page;
import nz.co.spark.components.services.DeviceDetailService;
import nz.co.tnz.base.core.compfactory.di.JcrDesignProperty;
import nz.co.tnz.base.core.compfactory.di.JcrProperty;
import nz.co.tnz.base.core.compfactory.di.ServiceReference;
import nz.co.tnz.base.core.components.SelfServiceComponent;
import nz.co.tnz.base.core.utils.Constants;
import nz.co.tnz.base.services.HybrisRequestHeader;
import nz.co.tnz.base.services.PagePathConstants;

/**
 * This class implements the Device Detail Page, which represents the Products.
 * 
 * @author Mahesh Kharvi
 * 
 */

public class DeviceDetailPage extends SelfServiceComponent {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceDetailPage.class);
       
    /** constants **/
    private String DEFAULT_DEVICES = "1";

    private String TRADEUP_BUNDLE_ID = "tradeUpBundleId";

    public static final String PATH_DEFAULT_OPTION_PAGE =
            "/content/cms/sample/en/store/checkoutpages/tnzproducts/checkout.options";

    public static final String COMPARE_PAGE_URL = "/shop/mobile/phones/comparepage.html";

    @ServiceReference
    private DeviceDetailService deviceDetailService;
    
    /** Commerce **/
    protected CommerceService commerceService;
    protected HybrisSession hybrisSession;

    /** params needed to handle requests **/
    private boolean isMBB = false;
    private String interestFreeTerm = "";
    private String INTEREST_FREE_PARAM = "intfree";
    private Page miniCartData = null;
    private String casualRate = "";
    protected String baseStore;
    private int cartEntryIndex = 0;

    /** classification data for overview page **/
    private ClassificationTO classificationBattery;
    private ClassificationTO classificationCamera;
    private ClassificationTO classificationScreen;
    private boolean classificationAvailable;
    private Map<String, List<ClassificationTO>> classificationMap;

    private HybrisRequestHeader hybrisRequestHeader;

    /** trade up related **/
    private String tradeUpBundleId = null;
    private boolean tradeUpJourney = false;


    private String planType = null;

    /** data to be transferred to prop **/
    private String deviceDetailListJson = null;
    protected List<DeviceDetailListTO> deviceDetailList = new ArrayList<>();
    private List<String> productsList = new ArrayList<>();
    private String baseProductSKU = null;
    private String seoDataJson = null;


    @Override
    public void init() {
        
        this.hybrisRequestHeader = this.getHybrisRequestHeader();
        this.linkOptionPage = this.getStringProperty("linkOptionPage", PATH_DEFAULT_OPTION_PAGE);
        this.addToCartRedirect = StringUtils.isEmpty(this.addToCartRedirect) ? PATH_DEFAULT_OPTION_PAGE
                        : this.addToCartRedirect;
        this.cartErrorRedirect = StringUtils.isEmpty(this.cartErrorRedirect) ? PATH_DEFAULT_OPTION_PAGE
                        : this.cartErrorRedirect;
        this.isMBB = BooleanUtils.toBoolean(getRequest().getParameter("data")) ? true : false;

        this.planType = StringUtils.isEmpty(getRequest().getParameter("planType")) ? "" : this.planType;
        this.casualRate = StringUtils.isEmpty(getRequest().getParameter(Constants.KEY_CASUALRATE)) ? "" : this.casualRate;

        if (this.getRequest().getParameter(TRADEUP_BUNDLE_ID) != null) {
            this.setTradeUpBundleId(this.getRequest().getParameter(TRADEUP_BUNDLE_ID));
            this.setTradeUpJourney(true);
        }

        if (this.getRequest().getParameter(INTEREST_FREE_PARAM) != null) {
            this.interestFreeTerm = this.getRequest().getParameter(INTEREST_FREE_PARAM).replaceAll("[a-zA-Z\\s]", "");
        }

        this.addToCartRedirect = this.getResourceResolver().map(
                this.addToCartRedirect);
        this.cartErrorRedirect = this.getResourceResolver().map(
                this.cartErrorRedirect);


        Session session = null;
        try {
            // this.miniCartData = this.hybrisSession.getMiniCart(this.hybrisRequestHeader);
            this.deviceDetailList = deviceDetailService.getDeviceDetailData(this.getRequest(), hybrisRequestHeader,
                    this.getResponse());

            if (this.deviceDetailList != null) {
                for (DeviceDetailListTO deviceDetailListTO : this.deviceDetailList) {
                    this.productsList.add(deviceDetailListTO.getDeviceId());
                    if (deviceDetailListTO.getBaseProductSKU() != null) {
                        this.baseStore = deviceDetailListTO.getBaseStore();
                        this.baseProductSKU = deviceDetailListTO.getBaseProductSKU();
                        this.classificationBattery = deviceDetailListTO.getClassificationBattery();
                        this.classificationCamera = deviceDetailListTO.getClassificationCamera();
                        this.classificationScreen = deviceDetailListTO.getClassificationScreen();
                        this.classificationAvailable = deviceDetailListTO.isClassificationAvailable();
                        this.classificationMap = deviceDetailListTO.getClassificationMap();
                    }
                }
                Gson gson = new Gson();
                this.deviceDetailListJson = gson.toJson(this.deviceDetailList);
                this.seoDataJson = gson.toJson(generateSEOData(this.deviceDetailList));
                LOG.info(this.deviceDetailListJson);
            }
        } catch (RepositoryException e) {
            LOG.error("when execute retrieveDeviceDetailTO: " + e);
        } catch (Exception e) {
            LOG.error("Exception", e);
        } finally {
            if (session != null) {
                session.logout();
            }
        }
    }

    private SeoDataTO generateSEOData(final List<DeviceDetailListTO> deviceDetailList) {
        SeoDataTO seoData = new SeoDataTO();
        TreeMap<String, SeoOffersTO> seoOffersTOList = new TreeMap<>();
        for (DeviceDetailListTO deviceDetailListTo : deviceDetailList) {
            seoData.setContext("http://schema.org/");
            seoData.setType("Product");
            seoData.setName(deviceDetailListTo.getManufacturer());
            seoData.setCategory(deviceDetailListTo.getCategory());
            seoData.setDescription(deviceDetailListTo.getShortDescription());
            SeoOffersTO seoOffersTO = new SeoOffersTO();
            seoOffersTOList.put("offers", generatSeoOffersData(deviceDetailListTo, seoOffersTO));
        }
        seoData.setOffers(seoOffersTOList);
        return seoData;
    }
    

    private SeoOffersTO generatSeoOffersData(final DeviceDetailListTO deviceDetailListTo,
                    final SeoOffersTO seoOffersTO) {
        seoOffersTO.setType("Offer");
        seoOffersTO.setAvailability("https://schema.org/InStock");
        seoOffersTO.setItemCondition("New Condition");
        seoOffersTO.setInventoryLevel(100);
        seoOffersTO.setPriceCurrency("NZD");
        seoOffersTO.setPrice(generateSEOPrice(deviceDetailListTo));
        seoOffersTO.getItemOffered().setType("Product");
        seoOffersTO.getItemOffered().getBrand().setName(StringUtils.isNotEmpty(deviceDetailListTo.getManufacturer()) ? deviceDetailListTo.getManufacturer() : "");
        seoOffersTO.getItemOffered().getBrand().setType("Thing");
        seoOffersTO.setModel(StringUtils.isNotEmpty(deviceDetailListTo.getDeviceTitle()) ? deviceDetailListTo.getDeviceTitle() : "");
        seoOffersTO.setSku(StringUtils.isNotEmpty(deviceDetailListTo.getDeviceId()) ? deviceDetailListTo.getDeviceId() : "");
        seoOffersTO.setColor(null != deviceDetailListTo.getOriginalProductColour() ? deviceDetailListTo.getOriginalProductColour().getName() : "");
        return seoOffersTO;
    }

    /** @param deviceDetailTO
     * @param deviceDetailListTO seo price is the lowest price a device can be bought from spark. So
     *        the price is set to default serviceplan price + device price for 24 months */
    private BigDecimal generateSEOPrice(final DeviceDetailListTO deviceDetailListTO) {
        BigDecimal seoPrice = null;
        if (null != deviceDetailListTO.getDeferPaymentList() && null != deviceDetailListTO.getCurrentPostpaidPlan()) {
            List<DeferredPaymentTO> defPayList =
                    deviceDetailListTO.getDeferPaymentList().get(
                            deviceDetailListTO.getCurrentPostpaidPlan());
            for (DeferredPaymentTO defPay : defPayList) {
                if (Integer.parseInt(defPay.getLength()) == 24) {
                    seoPrice = deviceDetailListTO.getDevicePlanPriceAmount();
                    seoPrice.add(defPay.getDeferredPayment().getAmount());
                }
            }
        }
        return seoPrice;
    }

    /**
        * This method is used to return the name of the css class for the frequency tab headers
        * 
        * @return
        */

    public String getAnalyticsData() {
        Map<String, List<String>> analyticsDataArray = new LinkedHashMap<String, List<String>>();        
        final List<String> eventList = Arrays.asList("event15", "event16", "event17", "purchase");
        analyticsDataArray.put("s.events", eventList);
        List<String> sProducts = new ArrayList<String>();
        analyticsDataArray.put("s.products", sProducts);
        Gson gson = new Gson();
        return gson.toJson(analyticsDataArray);
    }
           
    public String getDeviceDetailListJson() {
        return deviceDetailListJson;
    }

    public void setDeviceDetailListJson(final String deviceDetailListJson) {
        this.deviceDetailListJson = deviceDetailListJson;
    }

    public String getTradeUpBundleId() {
        return tradeUpBundleId;
    }

    public void setTradeUpBundleId(final String tradeUpBundleId) {
        this.tradeUpBundleId = tradeUpBundleId;
    }

    public boolean isTradeUpJourney() {
        return tradeUpJourney;
    }

    public void setTradeUpJourney(final boolean tradeUpJourney)
    {
        this.tradeUpJourney = tradeUpJourney;
    }

    public String getComparePageURL() {
        return COMPARE_PAGE_URL;
    }

    public String getInterestFreeTerm() {
        return interestFreeTerm;
    }

    public List<String> getProductsList() {
        return productsList;
    }

    public void setProductsList(final List<String> productsList)
    {
        this.productsList = productsList;
    }

    public String getProceedToCheckoutLink() {
        return PagePathConstants.REDIRECT_SHOP_CHECKOUT_TO;
    }

    // 5673: Tab hiding on device details page
    public String getIsTechDetailsTabHidden() {
        if (null != this.getCurrentPage() && null != this.getCurrentPage().getProperties()) {

            return (String) this.getCurrentPage().getProperties().get("isTechDetailsTabHidden");
        }
        return "";
    }

    public String getIsWhatsInTheBox() {
        if (null != this.getCurrentPage() && null != this.getCurrentPage().getProperties()) {
            return (String) this.getCurrentPage().getProperties().get("isWhatsInTheBox");
        }
        return "";
    }

    public String getPaymentOptionPath()
    {
        if (null != this.getCurrentPage() && null != this.getCurrentPage().getProperties()) {
            return (String) this.getCurrentPage().getProperties().get("paymentOptions");
        }
        return "";
    }


    public String getBoxImageReference() {
        return this.boxImageReference;
    }

    public String getLinkOptionPage() {
        return this.linkOptionPage;
    }


    public String getCartErrorRedirect() {
        return this.cartErrorRedirect;
    }

    public String getAddToCartRedirect() {
        return this.addToCartRedirect;
    }

    public String getIsZoomEnabled() {
        return this.getCurrentPage().getProperties().get("zoomEnabled", "false");
    }

    public List<DeviceDetailListTO> getDeviceDetailList() {
        return deviceDetailList;
    }

    public void setDeviceDetailList(final List<DeviceDetailListTO> deviceDetailList) {
        this.deviceDetailList = deviceDetailList;
    }

    public String getBaseStore() {
        return baseStore;
    }

    public String getNumberOfDevices() {
        return this.getCurrentPage().getProperties().get("numberOfDevices", DEFAULT_DEVICES);
    }


    public ClassificationTO getClassificationBattery() {
        return classificationBattery;
    }


    public void setClassificationBattery(final ClassificationTO classificationBattery) {
        this.classificationBattery = classificationBattery;
    }


    public ClassificationTO getClassificationCamera() {
        return classificationCamera;
    }


    public void setClassificationCamera(final ClassificationTO classificationCamera) {
        this.classificationCamera = classificationCamera;
    }


    public ClassificationTO getClassificationScreen() {
        return classificationScreen;
    }


    public void setClassificationScreen(final ClassificationTO classificationScreen) {
        this.classificationScreen = classificationScreen;
    }

    public boolean isClassificationAvailable() {
        return classificationAvailable;
    }

    public void setClassificationAvailable(final boolean classificationAvailable) {
        this.classificationAvailable = classificationAvailable;
    }

    public Map<String, List<ClassificationTO>> getClassificationMap() {
        return classificationMap;
    }

    public void setClassificationMap(final Map<String, List<ClassificationTO>> classificationMap) {
        this.classificationMap = classificationMap;
    }

    public String getBaseProductSKU() {
        return baseProductSKU;
    }

    public void setBaseProductSKU(final String baseProductSKU) {
        this.baseProductSKU = baseProductSKU;
    }

    public String getSameAsURL() {
        return sameAsURL;
    }

    public void setSameAsURL(final String sameAsURL) {
        this.sameAsURL = sameAsURL;
    }


    public String getSeoDataJson() {
        return seoDataJson;
    }

    public void setSeoDataJson(final String seoDataJson) {
        this.seoDataJson = seoDataJson;
    }


    public boolean isMBB() {
        return isMBB;
    }


    public void setMBB(final boolean isMBB) {
        this.isMBB = isMBB;
    }


    public Page getMiniCartData() {
        return miniCartData;
    }


    public void setMiniCartData(final Page miniCartData) {
        this.miniCartData = miniCartData;
    }


    public String getCasualRate() {
        return casualRate;
    }


    public void setCasualRate(final String casualRate) {
        this.casualRate = casualRate;
    }


    public String getPlanType() {
        return planType;
    }


    public void setPlanType(final String planType) {
        this.planType = planType;
    }


    public List<CartEntry> getMiniCartEntries() {
        List<CartEntry> result = null;
        if ((this.miniCartData != null) && (this.miniCartData.getCart() != null)
                && (this.miniCartData.getCart().getCartEntries() != null)) {
            result = this.miniCartData.getCart().getCartEntries().getCartEntry();
            LOG.info("*****DDP: size of minicart getCrtEntry(): " + result.size());
        }
        return result;
    }

    public int getMiniCartNumEntries() {
        int result = 0;
        if ((this.miniCartData != null) && (this.miniCartData.getCart() != null)
                && (this.miniCartData.getCart().getCartEntries() != null)
                && (this.miniCartData.getCart().getCartEntries().getCartEntry() != null)) {
            result = this.miniCartData.getCart().getCartEntries().getCartEntry().size();
        }
        return result;
    }

    public List<String> getSortedEntryForMiniCart() {
        List<String> sortedEntry = new ArrayList<String>();
        // this list contains componentypes
        sortedEntry.add("Phone");
        sortedEntry.add("SERVICE_PLAN");
        sortedEntry.add("Extras");

        // adding componentType incase of favourites
        sortedEntry.add("FAVOURITE_SERVICE_ADDON");
        sortedEntry.add("SIM_CARD");
        sortedEntry.add("Insurance");
        sortedEntry.add("Gift");
        sortedEntry.add("charge");
        sortedEntry.add("Credit");
        sortedEntry.add("ACCESSORY");

        if ((this.miniCartData != null) && (this.miniCartData.getCart() != null)
                && (this.miniCartData.getCart().getCartEntries() != null)
                && (this.miniCartData.getCart().getCartEntries().getCartEntry() != null)) {
            List<CartEntry> listCartEntry = this.miniCartData.getCart().getCartEntries().getCartEntry();
            for (int j = 0; j < listCartEntry.size() && cartEntryIndex < listCartEntry.size(); j++) {
                CartEntry cartEntry = listCartEntry.get(cartEntryIndex);

                if (cartEntry != null && cartEntry.getHighlightProductType() != null) {
                    String highlightProductType = cartEntry.getHighlightProductType();
                    if (highlightProductType.equalsIgnoreCase("sim")) {
                        for (int index = 0; index < sortedEntry.size(); index++) {
                            if (sortedEntry.get(index).equalsIgnoreCase("sim_card")) {
                                sortedEntry.remove(index);
                            }
                        }
                        sortedEntry.set(0, "SIM_CARD");
                        cartEntryIndex++;
                        return sortedEntry;
                    } else if (highlightProductType.equalsIgnoreCase("accessory")) {
                        for (int index = 0; index < sortedEntry.size(); index++) {
                            if (sortedEntry.get(index).equalsIgnoreCase("accessory")) {
                                sortedEntry.remove(index);
                            }
                        }
                        sortedEntry.set(0, "Accessory");
                        cartEntryIndex++;
                        return sortedEntry;
                    } else {
                        cartEntryIndex++;
                        return sortedEntry;
                    }

                }
            }
        }

        return sortedEntry;

    }


    public String getPromo12Month() {
        return promo12Month;
    }


    public void setPromo12Month(final String promo12Month) {
        this.promo12Month = promo12Month;
    }


    public String getPromoMessage12Month() {
        return promoMessage12Month;
    }


    public void setPromoMessage12Month(final String promoMessage12Month) {
        this.promoMessage12Month = promoMessage12Month;
    }


    public String getPromo24Month() {
        return promo24Month;
    }


    public void setPromo24Month(final String promo24Month) {
        this.promo24Month = promo24Month;
    }


    public String getPromoMessage24Month() {
        return promoMessage24Month;
    }


    public void setPromoMessage24Month(final String promoMessage24Month) {
        this.promoMessage24Month = promoMessage24Month;
    }
}

