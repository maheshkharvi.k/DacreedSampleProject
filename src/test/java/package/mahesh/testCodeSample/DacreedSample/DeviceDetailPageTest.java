package nz.co.tnz.components.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.CommerceSession;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.commons.WCMUtils;

import junitx.util.PrivateAccessor;
import nz.co.spark.base.services.myspark.MySparkLoggedInMiscService;
import nz.co.spark.base.services.myspark.SessionService;
import nz.co.spark.base.services.transferobjects.SessionState;
import nz.co.spark.commerce.hybris.SessionInfo;
import nz.co.spark.commerce.hybris.common.DefaultHybrisSession;
import nz.co.spark.commerce.hybris.common.HybrisSessionInfo;
import nz.co.spark.commerce.hybris.to.BundleTabTO;
import nz.co.spark.commerce.hybris.to.ClassificationTO;
import nz.co.spark.commerce.hybris.to.DeviceDetailListTO;
import nz.co.spark.commerce.hybris.to.DeviceDetailTO;
import nz.co.spark.components.services.AddToCartService;
import nz.co.spark.components.services.DeviceDetailService;
import nz.co.spark.components.services.ShopLoginService;
import nz.co.tnz.base.core.components.AbstractComponent;
import nz.co.tnz.base.core.components.CookiesHandler;
import nz.co.tnz.base.core.components.SelfServiceComponent;
import nz.co.tnz.base.services.HybrisRequestHeader;
import nz.co.tnz.base.services.HybrisRequestHeader.Builder;
import nz.co.tnz.components.servlet.CustomerInformationServlet;

@PrepareForTest({FrameworkUtil.class})
public class DeviceDetailPageTest {

    @InjectMocks
    private DeviceDetailPage deviceDetailPage;

    @Mock
    HybrisRequestHeader hybrisRequestHeader1;

    @Mock
    HybrisRequestHeader hybrisRequestHeader;

    @Mock
    SelfServiceComponent selfServiceComponent;

    @Mock
    CookiesHandler cookiesHandler;

    @Mock
    AbstractComponent abstractComponent;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    ShopLoginService shopLoginService;

    @Mock
    CustomerInformationServlet customerInformationServlet;

    @Mock
    AddToCartService addToCartService;

    @Mock
    DeviceDetailTO deviceDetailTO;

    @Mock
    Bundle bundle;

    @Mock
    BundleContext bundleContext;
    @Mock
    ServiceReference serviceReference;

    @Mock
    MySparkLoggedInMiscService mySparkLoggedInMiscService;

    @Mock
    SessionService sessionService;

    @Mock
    SessionState sessionState;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private Builder builder;

    @Mock
    ComponentContext componentContext;

    @Mock
    ResourceResolver resourceResolver;
    @Mock
    public Resource resource;
    @Mock
    ValueMap properties;
    @Mock
    PageManager pageManager;
    @Mock
    Page page;

    @Mock
    Cookie cookie;

    @Mock
    CommerceService commerceService;

    @Mock
    BundleTabTO bundleTabTO;


    @Mock
    DeviceDetailService deviceDetailService;


    Class<AddToCartService> fieldType;
    public static final String INTERNET_REDIRECT = "/shop/internet";
    private static Map<String, String> cookieInfo;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        builder = new Builder();
        deviceDetailPage.setRequest(request);
        deviceDetailPage.setResponse(response);
        fieldType = AddToCartService.class;
        PowerMockito.mockStatic(FrameworkUtil.class);
        PrivateAccessor.setField(deviceDetailPage, "cookiesHandler", cookiesHandler);

        when(request.getSession()).thenReturn(session);
        when(WCMUtils.getComponentContext(request)).thenReturn(componentContext);
        when(componentContext.getPage()).thenReturn(page);
        when(request.getResource()).thenReturn(resource);

        when(resource.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.isLive()).thenReturn(true);
        when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        when(abstractComponent.getResource()).thenReturn(resource);
        when(page.getProperties()).thenReturn(properties);
        cookieInfo = new HashMap<String, String>();
        cookieInfo.put("mockSessionInfo", "mockSessionInfo");
    }

    @Test
    public void testGetCameraClassification_Not_Null() throws Exception {

        ClassificationTO classificationTO = new ClassificationTO();
        List<DeviceDetailListTO> deviceDetailList = new ArrayList<>();
        DeviceDetailListTO deviceDetailListTO = new DeviceDetailListTO();
        classificationTO.setCode("cameraclassification");

        deviceDetailListTO.setClassificationCamera(classificationTO);
        deviceDetailListTO.setBaseProductSKU("Device399");
        deviceDetailList.add(deviceDetailListTO);
        when(deviceDetailService.getDeviceDetailData(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(deviceDetailList);
        deviceDetailPage.init();
        ClassificationTO classificationCamera = deviceDetailPage.getClassificationCamera();
        assertNotNull(classificationCamera);
        assertEquals("cameraclassification", classificationCamera.getCode());
    }

    @Test
    public void testGetCameraClassification_Null() throws Exception {

        ClassificationTO classificationTO = new ClassificationTO();
        List<DeviceDetailListTO> deviceDetailList = new ArrayList<>();
        DeviceDetailListTO deviceDetailListTO = new DeviceDetailListTO();
        classificationTO.setCode("cameraclassification");
        deviceDetailListTO.setClassificationCamera(classificationTO);
        deviceDetailList.add(deviceDetailListTO);
        when(deviceDetailService.getDeviceDetailData(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(deviceDetailList);
        deviceDetailPage.init();
        ClassificationTO classificationCamera = deviceDetailPage.getClassificationCamera();
        assertNull(classificationCamera);
    }


    @Test
    public void testGetDevicedetail_Json_Empty() throws Exception {
        List<DeviceDetailListTO> deviceDetailList = new ArrayList<>();
        DeviceDetailListTO deviceDetailListTO = new DeviceDetailListTO();
        deviceDetailListTO.setBaseProductSKU("Device399");
        deviceDetailList.add(deviceDetailListTO);
        when(deviceDetailService.getDeviceDetailData(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(deviceDetailList);
        deviceDetailPage.init();
        String result =
                deviceDetailPage.getDeviceDetailListJson();
        DeviceDetailListTO deviceDetailListTo =
                deviceDetailPage.getDeviceDetailList().get(0);
        assertNotNull(deviceDetailListTo);
        assertEquals("Device399", deviceDetailListTo.getBaseProductSKU());
        assertEquals(false, result.isEmpty());
    }

    @Test

    @Ignore
    public void testInit() {
        when(request.getHeader("User-Agent")).thenReturn("User-Agent");
        cookiesHandler.setLoadBalancerCookie(request, response, "value1");
        when(cookiesHandler.getLoadBalancerCookie(request)).thenReturn("value1");
        deviceDetailPage.init();
    }

    private DefaultHybrisSession getHybrisSession(CommerceService service, Map<String, String> cookieInfo) {
        SessionInfo sessionInfo = mock(HybrisSessionInfo.class);
        when(sessionInfo.getUserId()).thenReturn("test_user");
        when(sessionInfo.getCookieInfo()).thenReturn(cookieInfo);

        DefaultHybrisSession session = mock(DefaultHybrisSession.class,
                withSettings().extraInterfaces(CommerceSession.class));
        when(session.getSessionInfo()).thenReturn(sessionInfo);
        when(session.getCommerceService()).thenReturn(service);
        return session;
    }



    @Test
    public void testGetSeoData_Not_Empty() throws Exception {
        List<DeviceDetailListTO> deviceDetailList = new ArrayList<>();
        DeviceDetailListTO deviceDetailListTO = new DeviceDetailListTO();
        deviceDetailListTO.setBaseProductSKU("Device399");
        deviceDetailListTO.setCategory("Phones");
        deviceDetailListTO.setManufacturer("iphone");
        deviceDetailListTO.setShortDescription("this is a phone buy journey");
        deviceDetailList.add(deviceDetailListTO);
        when(deviceDetailService.getDeviceDetailData(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(deviceDetailList);
        deviceDetailPage.init();
        String resultList =
                deviceDetailPage.getSeoDataJson();
        assertEquals(false, resultList.isEmpty());
    }
}
